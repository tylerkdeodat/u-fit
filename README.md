# Workout/Fitness Project

* Tyler Deodat
* Spencer Rhyne
* Tony Tan
* Ethan Williams

## Design
*[API Design](docs/apis.md)
*[Data Model](docs/data-model.md)
*[GHI](docs/ghi.md)
*[Integrations](docs/integrations.md)

## Intended Market:

This applications will be geared towards people joining a gym and wanting to structure their own workouts as well as have the option to schedule workouts with a personal trainer.

## Functionality

Clients will be able to access a vast api of exercise information that they can use to create their own workout plan. This plan can be saved and each exercise can be viewed individually. They can set up how many reps/sets to do. They can also sign up and gain access for training with a personal trainer. View/edit/cancel appointments.

## Install packages

npm i jwt-decode
npm install @mui/material @emotion/react @emotion/styled
npm i react-horizontal-scrolling-menu

## Data creation

Data for data creation lives in [API Design](docs/apis.md).
When creating data that has a nested data object, the id is required to create the data accurately. Data still creates, but will not proc previously inputted data.


## Setup

For navigating the site from an admin side please create .env file in /ghi and add the following 2 lines to it:

REACT_APP_EXERCISE_API_KEY = 7e1bc4fc56msh629e8ca2f5ab38ep1cbce4jsn8941aa48ecbb

REACT_APP_EXERCISE_HOST = http://localhost:8000