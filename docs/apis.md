# APIs

[Link](https://wger.de/en/software/api)
[Link](https://rapidapi.com/justin-WFnsXH_t6/api/exercisedb/)
[Link](https://youtube-search-and-download.p.rapidapi.com/search)
[Link](https://exercisedb.p.rapidapi.com/exercises/bodyPartList)


Nested data, from another input, will need the id of the nested data for data creation.

## Client

* **Method**: `POST`, `GET`, `GET`, `PUT`
* **Path**: /fitness-model/client, /fitness-model/client/<int:id>

Input:

```json
{
   "username": "string",
    "password": "string",
    "name": "string",
    "email": "string",
    "phone_number": "string",
    "age": "string",
    "height": "string",
    "current_weight": "string",
    "goal_weight": "string",
    "gender": "string"
}
```

Output:
```json
{
    "id": "string",
    "username": "string",
    "hashed_password": "string",
    "name": "string",
    "email": "string",
    "phone_number": "string",
    "age": "string",
    "height": "string",
    "current_weight": "string",
    "goal_weight": "string",
    "gender": "string"
}
```

## Gyms


* **Method**: `POST`, `GET`, `GET`, `PUT`
* **Path**: /fitness-model/gyms, /fitness-model/gyms/<int:id>


Input:

```json
{
    "name": "string",
    "location": "string",
}

Output:
```json
{
    "name": "string",
    "location": "string",
    "id": "string"
}
```

## Trainer


* **Method**: `POST`, `GET`, `GET`, `PUT`
* **Path**: /fitness-model/trainer, /fitness-model/trainer/<int:id>

Input:

```json
{
    "name": "string",
    "email": "string",
    "phone_number": "string",
    "age": 0,
    "gender": "string",
    "gym": {
        "name": "string",
        "location": "string",
        "id": "string"
    },
}
```

Output:
```json
{
    "name": "string",
    "email": "string",
    "phone_number": "string",
    "age": 0,
    "gender": "string",
    "gym": {
        "name": "string",
        "location": "string",
        "id": "string"
    },
    "id": "string"
}
```

## Appointment


* **Method**: `POST`, `GET`, `GET`, `PUT`
* **Path**: /fitness-model/appointment, /fitness-model/appointment/<int:id>

Input:

```json
{
    "time": "string",
    "location": "string",
    "date": "string",
    "duration": "string",
    "client": {
        "id": "string",
        "username": "string",
        "hashed_password": "string",
        "name": "string",
        "email": "string",
        "phone_number": "string",
        "age": "string",
        "height": "string",
        "current_weight": "string",
        "goal_weight": "string",
        "gender": "string"
    },
    "trainer": {
        "name": "string",
        "email": "string",
        "phone_number": "string",
        "age": 0,
        "gender": "string",
        "gym": {
            "name": "string",
            "location": "string",
            "id": "string"
        },
        "id": "string"
    },
}
```

Output:
```json
{
    "time": "string",
    "location": "string",
    "date": "string",
    "duration": "string",
    "client": {
        "id": "string",
        "username": "string",
        "hashed_password": "string",
        "name": "string",
        "email": "string",
        "phone_number": "string",
        "age": "string",
        "height": "string",
        "current_weight": "string",
        "goal_weight": "string",
        "gender": "string"
    },
    "trainer": {
        "name": "string",
        "email": "string",
        "phone_number": "string",
        "age": 0,
        "gender": "string",
        "gym": {
            "name": "string",
            "location": "string",
            "id": "string"
        },
        "id": "string"
    },
    "id": "string"
}
```


## Dates(Days open)


* **Method**: `POST`, `GET`, `GET`, `PUT`
* **Path**: /fitness-model/dates, /fitness-model/dates/<int:id>


Input:

```json
{
    "monday": "string",
    "tuesday": "string",
    "wednesday": "string",
    "thursday": "string",
    "friday": "string",
    "saturday": "string",
    "sunday": "string",
    "gym": {
        "name": "string",
        "location": "string",
        "id": "string"
    },
}

Output:
```json
{
    "monday": "string",
    "tuesday": "string",
    "wednesday": "string",
    "thursday": "string",
    "friday": "string",
    "saturday": "string",
    "sunday": "string",
    "gym": {
        "name": "string",
        "location": "string",
        "id": "string"
    },
    "id": "string"
}
```

## Times(Times open)


* **Method**: `POST`, `GET`, `GET`, `PUT`
* **Path**: /fitness-model/times, /fitness-model/times/<int:id>


Input:

```json
{
   "monday": [
        "string"
    ],
    "tuesday": [
        "string"
    ],
    "wednesday": [
        "string"
    ],
    "thursday": [
        "string"
    ],
    "friday": [
        "string"
    ],
    "saturday": [
        "string"
    ],
    "sunday": [
        "string"
    ],
    "gym": {
        "name": "string",
        "location": "string",
        "id": "string"
    },
}

Output:
```json
{
   "monday": [
        "string"
    ],
    "tuesday": [
        "string"
    ],
    "wednesday": [
        "string"
    ],
    "thursday": [
        "string"
    ],
    "friday": [
        "string"
    ],
    "saturday": [
        "string"
    ],
    "sunday": [
        "string"
    ],
    "gym": {
        "name": "string",
        "location": "string",
        "id": "string"
    },
    "id": "string"
}
```

## Exercise

* **Method**: `POST`, `GET`, `GET`, `PUT`
* **Path**: /fitness-model/exercise, /fitness-model/exercise/<int:id>

Input:

```json
{
    "name": "string",
    "sets": "string",
    "reps": "string",
    "gif-url": "string",
}
```

Output:
```json
{
    "id": "string",
    "name": "string",
    "sets": "int",
    "reps": "int",
    "gif-url": "string",
}
```