## Data Models

## Workout Microservice
## Workout
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
|name| string | no | no |
|sets| string | no | no |
|reps| string | no | no |
|current_exercise| dictionary | no | no |


## Exercise

| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
|bodyPart| string | no | no |
| equipment | string | no | no |
| gifUrl | string | no | no |
| name | string | yes | no |
| target| string | no | yes |

The `exercise` entity contains the information about an individual exercise to be preformed.


## Client

| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
| name | string | no | yes |
| username | string | no | no |
| password | string | no | no |
| email | string | no | yes |
| phone number | string | no | yes |
| age | int | no | yes |
| height | string | no | yes |
| current_weight | string | no | yes |
| goal_weight | string | no | yes |
| gender | string | no | yes |

The `client` entity contains the information about an individual that is signing up for the app.

## Trainer

| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
| name | string | no | no |
| email | string | no | no |
| phone number | string | no | no |
| age | int | no | yes |
| gender | string | no | yes |
| gym | dictionary | no | no |


The `trainer` entity contains the information about an individual that is working for the app.

## Appointment

| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
| time | string | no | no |
| location | string | no | no |
| date | string | no | no |
| duration | int | no | no |
| client | dictionary | no | no |
| trainer | dictionary | no | no |

The `appointment` entity contains the information about an individual appointment that has been scheduled between a client and a trainer.

## Gym

| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
| name | string | no | no |
| location | string | no | no |

The `gym` entity contains the information about a gym that will house trainers.


## DaysOpen

| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
| monday | string | no | no |
| tuesday | string | no | no |
| wednesday | string | no | no |
| thursday | string | no | no |
| friday | string | no | no |
| saturday | string | no | no |
| sunday | string | no | no |
| gym | dictionary | no | no |

The `DaysOpen` entity contains the information about days that the associated gym will be open.

## TimeOpen

| Name | Type | Unique | Optional |
|-|-|-|-|
| id | string | yes | no |
| monday | tuple | no | no |
| tuesday | tuple | no | no |
| wednesday | tuple | no | no |
| thursday | tuple | no | no |
| friday | tuple | no | no |
| saturday | tuple | no | no |
| sunday | tuple | no | no |
| gym | dictionary | no | no |

The `TimeOpen` entity contains the information about times that the associated gym will be open. Information is a tuple so an open and closed time can be created.