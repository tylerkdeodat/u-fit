## Graphical Human Interface

## Homepage

 This is the main page that people will see when they get to the web site.

 ![home page](wireframes/home-page.png)

## Login Page

 This is the page that users will see when they login in or sign up.

 ![login page](wireframes/login-page.png)

## Appointment Page(stretch)

 This will be the page for creating an appointment with a personal trainer. Only shown to those who have created an account.

 ![Appointment page](wireframes/Create-appointment.png)

## Select An Exercise

 This page will show a list of exercises by either name, body part, or equipment. With the option to add it to a workout plan. THat will redirect non-logged in users to a sigin/sign up page.

 ![select a workout](wireframes/select-exercise.png)

## Exercise Details

 This page is shown when clicking on an individual exercise. Showing a gif of that exercise on how to do it along with a short description.

 ![exercise details](wireframes/exercise-details.png)

## My Workout(stretch)

 This page will show a workout routine that is a list of exercises that the user has picked.

 ![my workout](wireframes/my-workout.png)

