from distutils.log import error
from http import client
from unicodedata import name
from unittest import result
from pymongo import MongoClient, ASCENDING
from pymongo.errors import DuplicateKeyError
import os
import json
from bson.objectid import ObjectId
from models.trainer import TrainerOut

from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password):
    print("pass",password)
    return pwd_context.hash(password)


dbhost = os.environ["MONGOHOST"]
dbname = os.environ["MONGODATABASE"]
dbuser = os.environ["MONGOUSER"]
dbpass = os.environ["MONGOPASSWORD"]
dbsecret = os.environ["SIGNING_KEY"]

connection_string = "mongodb://{}:{}@{}/{}?authSource=admin".format(dbuser, dbpass, dbhost, dbname)

client = MongoClient(connection_string)
db = client[dbname]

class DuplicateTitle(RuntimeError):
    pass

# client = MongoClient(connection_string, { useUnifiedTopology: true })

class TrainerQueries:
    def get_all_trainers(self):
        print("step 2")
        trainers = list(
            db.trainers.find()
                .sort("name")
        )
        print("trainer step 2:", trainers)
        return list(trainers)

    def get_trainer(self, id: int):
        trainer = db.trainers.find_one({"_id": id });
        return trainer

    def insert_trainer(self, name, email, phone_number, age, gender, gym):
        print("insert_trainer")
        print(gym)
        try:
            gym = dict(gym)
            result = db.trainers.insert_one({
                "name": name,
                "email": email,
                "phone_number": phone_number,
                "age": age,
                "gender": gender,
                "gym": gym
            });
            trainer = self.get_trainer(result.inserted_id)
            trainer["id"] = str(trainer["_id"])
            del trainer["_id"]
            print("ROUTER:", trainer)
            return trainer
        except DuplicateKeyError:
            print(error)
            raise DuplicateKeyError(error)

    def update_trainer(self, id, name, email, phone_number, age, gender):
        try:
            result = db.trainers.update_one(
                {"_id": id },
                [{ "$set": { "name": name}},
                { "$set": { "email": email}},
                { "$set": { "phone_number": phone_number}},
                { "$set": { "age": age}},
                { "$set": { "gender": gender}}],
            )
            # print("result", result)
            if result.matched_count == 0:
                return None
            return self.get_trainer(id)
        except DuplicateKeyError:
            raise DuplicateTitle()

    # def delete_trainer(self, id):
    #     client = MongoClient(connection_string)
    #     db = client[dbname]
    #     db.trainers.delete_one({ "id": id})

class AppointmentQueries:
    def get_all_appointments(self):
        appointments = list(
            db.appointments.find()
                # .sort("client", ASCENDING)
                .limit(10)
        )
        return list(appointments)

    def get_appointment(self, id: int):
        print("step 3")
        print(id)
        appointment = db.appointments.find_one({"_id": id});
        print("here",appointment)
        # appointment["id"] = str(appointment["_id"])
        # del appointment["_id"]
        # client = db.clients.find_one({"_id": ObjectId(appointment["client"]["id"])})
        # appointment["client"] = client


        return appointment

    def insert_appointment(self, time, location, date, duration, client, trainer):
        #appointment_number = db.appointments.create_index([("appointment_number", pymongo.ASCENDING)], unique=True) gym = dict(gym)
        try:
            client = dict(client)
            trainer = dict(trainer)
            trainer["gym"] = dict(trainer["gym"])
            result = db.appointments.insert_one({
                "time": time,
                "location": location,
                "date": date,
                "duration": duration,
                "client": client,
                "trainer": trainer,
            });
            appointment =self.get_appointment(result.inserted_id)
            print(appointment)
            appointment["id"] = str(appointment["_id"])
            del appointment["_id"]
            return appointment
        except DuplicateKeyError:
            raise DuplicateTitle(error)

    def update_appointment(self, id, time, location, date, duration, client, trainer):
        try:
            print("step 2")
            print(client)
            result = db.appointments.update_one(
                { "_id": id},
                [{ "$set": {"time": time }},
                { "$set": {"location": location}},
                { "$set": {"date": date}},
                { "$set": {"duration": duration}},
                { "$set": {"client": client}},
                { "$set": { "trainer": trainer}},
                ],
            )

            if result.matched_count == 0:
                return None
            appointment = self.get_appointment(id)
            print("step 6",appointment)
            appointment["client"]["hashed_password"] = ""
            return appointment
        except DuplicateKeyError:
            raise DuplicateTitle()

    # def delete_appointment(self, id):
    #     client = MongoClient(connection_string)
    #     db = client[dbname]
    #     db.appointments.delete_one({ "id": id})

class ClientQueries:
    def get_all_clients(self):
        clients = list(
            db.clients.find()
                .sort("name")
        )

        for client1 in clients:
            client1["id"] = str(client1["_id"])
            del client1["_id"]
        print(clients)
        return list(clients)

    def get_client(self, id:int):
        client1 = db.clients.find_one({ "_id": id });
        return client1

    def get_client_in_db(self, username:str):
        user = db.clients.find_one({"username": username })
        print("user from clientQur",user)
        if user:
            user["id"] = str(user["_id"])
            del user["_id"]
            print("user from get_client_in_db", user)
            return user
        else:
            return None

    def insert_client(self, username, password, name, email, phone_number, age, height, current_weight, goal_weight, gender):
        if db.clients.find_one({"username": username }):
            print("username already exist")
            return;
        try:
            result = db.clients.insert_one({
                "username": username,
                "name": name,
                "email": email,
                "phone_number": phone_number,
                "age": age,
                "height": height,
                "current_weight": current_weight,
                "goal_weight": goal_weight,
                "gender": gender,
                "hashed_password": get_password_hash(password),
            });
            client1 = self.get_client(result.inserted_id)
            client1["id"] = str(client1["_id"])
            print("insert", client1)
            del client1["_id"]
            return client1
        except DuplicateKeyError:
            raise DuplicateKeyError(error)

    def update_client(self, id, username, name, email, phone_number, age, height, current_weight, goal_weight, gender):
        try:
            result = db.clients.update_one(
                {"_id": id},
                [{ "$set": { "username": username }},
                { "$set": { "name": name}},
                { "$set": { "email": email}},
                { "$set": { "phone_number": phone_number}},
                { "$set": { "age": age}},
                { "$set": { "height": height}},
                { "$set": { "current_weight": current_weight}},
                { "$set": { "goal_weight": goal_weight}},
                { "$set": { "gender": gender}}],
            )
            if result.matched_count == 0:
                return None
            return self.get_client(id)
        except DuplicateKeyError:
            raise DuplicateTitle()


    # def delete_client(self, id):
    #     client = MongoClient(connection_string)
    #     db = client[dbname]
    #     db.clients.delete_one({"id": id})


class AccountsQueries:
    def get_user(self, username: str):
        print("here")
        user = db.clients.find_one({"username": username })
        print("user from accoutQur",user)
        if user:
            user["id"] = str(user["_id"])
            del user["_id"]
            return user
        else:
            return None

    def insert_user(self, username, password):
        try:
            result = db.users.insert_one({
                "username": username,
                "password": password,

            });
            print(result)
            user = self.get_user(result.inserted_id)
            user["id"] = str(user["_id"])
            del user["_id"]
            return user
        except DuplicateKeyError:
            raise DuplicateKeyError(error)
#     def get_client(self, id: int):
#         client = MongoClient(connection_string)
#         db = client[dbname]
#         client1 = db.clients.find_one({"_id": id });
#         return client1


class GymQueries:
    def get_all_gyms(self):
        gyms = list(
            db.gyms.find()
                .sort("name")
        )
        for gym in gyms:
            gym["id"] = str(gym["_id"])
            del gym["_id"]
        print(gyms)
        return list(gyms)

    def get_gym(self, id:int):
        gym = db.gyms.find_one({ "_id": id });

        return gym

    def get_gym_in_db(self, id:str):
        client = MongoClient(connection_string)
        db = client[dbname]
        gym = db.gyms.find_one({"_id": id })
        print("gym from clientQur",gym)
        if gym:
            gym["id"] = str(gym["_id"])
            del gym["_id"]
            return gym
        else:
            return None

    def insert_gym(self, name, location,):
        try:
            result = db.gyms.insert_one({
                "name": name,
                "location": location,
            });
            gym = self.get_gym(result.inserted_id)
            gym["id"] = str(gym["_id"])
            print("insert", gym)
            del gym["_id"]
            return gym
        except DuplicateKeyError:
            raise DuplicateKeyError(error)

    def update_gym(self, id, name, location):
        try:
            result = db.gyms.update_one(
                {"_id": id},
                [{ "$set": { "name": name}},
                { "$set": { "location": location}}],
            )
            if result.matched_count == 0:
                return None
            return self.get_gym(id)
        except DuplicateKeyError:
            raise DuplicateTitle()


    # def delete_client(self, id):
    #     client = MongoClient(connection_string)
    #     db = client[dbname]
    #     db.clients.delete_one({"id": id})


class TimeQueries:
    def get_all_times(self):
        times = list(db.times.find())
        for time in times:
            gym = db.gyms.find_one({"_id": ObjectId(time["gym"]["id"])})
            gym["id"] = str(gym["_id"])
            time["gym"] = gym
        print(times)
        return list(times)

    def get_time(self, id:int):
        time = db.times.find_one({ "_id": id });
        return time

    def insert_time(self, monday, tuesday, wednesday, thursday, friday, saturday, sunday, gym):

        try:
            gym = dict(gym)
            result = db.times.insert_one({
                "monday": monday,
                "tuesday": tuesday,
                "wednesday": wednesday,
                "thursday": thursday,
                "friday": friday,
                "saturday": saturday,
                "sunday": sunday,
                "gym": gym
            });
            time = self.get_time(result.inserted_id)
            time["id"] = str(time["_id"])
            print("insert", time)
            del time["_id"]
            return time
        except DuplicateKeyError:
            raise DuplicateKeyError(error)
    def update_time(self, id, monday, tuesday, wednesday, thursday, friday, saturday, sunday, gym):
        try:
            result = db.times.update_one(
                {"_id": id},
                [{ "$set": { "monday": monday}},
                { "$set": { "tuesday": tuesday}},
                { "$set": { "wednesday": wednesday}},
                { "$set": { "thursday": thursday}},
                { "$set": {  "friday": friday}},
                { "$set": { "saturday": saturday}},
                { "$set": { "sunday": sunday}},
                { "$set": { "gym": gym}}],
            )
            if result.matched_count == 0:
                return None
            return self.get_time(id)
        except DuplicateKeyError:
            raise DuplicateTitle()
class DateQueries:
    def get_all_dates(self):
        dates = list(db.dates.find())
        print(dates)
        for date in dates:
            gym = db.gyms.find_one({"_id": ObjectId(date["gym"]["id"])})
            gym["id"] = str(gym["_id"])
            date["gym"] = gym
        print(dates)
        return list(dates)

    def get_date(self, id:int):
        date = db.dates.find_one({ "_id": id });
        return date

    def insert_date(self, monday, tuesday, wednesday, thursday, friday, saturday, sunday, gym):
        try:
            gym = dict(gym)
            result = db.dates.insert_one({
                "monday": monday,
                "tuesday": tuesday,
                "wednesday": wednesday,
                "thursday": thursday,
                "friday": friday,
                "saturday": saturday,
                "sunday": sunday,
                "gym": gym
            });
            date = self.get_date(result.inserted_id)
            date["id"] = str(date["_id"])
            print("insert", date)
            del date["_id"]
            return date
        except DuplicateKeyError:
            raise DuplicateKeyError(error)

    def update_date(self, id, monday, tuesday, wednesday, thursday, friday, saturday, sunday, gym):
        try:
            result = db.dates.update_one(
                {"_id": id},
                [{ "$set": { "monday": monday}},
                { "$set": { "tuesday": tuesday}},
                { "$set": { "wednesday": wednesday}},
                { "$set": { "thursday": thursday}},
                { "$set": {  "friday": friday}},
                { "$set": { "saturday": saturday}},
                { "$set": { "sunday": sunday}},
                { "$set": { "gym": gym}}],
            )
            if result.matched_count == 0:
                return None
            return self.get_date(id)
        except DuplicateKeyError:
            raise DuplicateTitle()

class WorkoutQueries:
    def get_all_workouts(self):
        print("step 2")
        workouts = list(
            db.workouts.find()
                .sort("name")
        )
        # for workout in workouts:
        #     workout["id"] = str(workout["_id"])
        #     # del workout["_id"]
        print("/n/n/n/", workouts, "/n/n/n/")
        return list(workouts)

    def get_workout(self, id:int):
        print("step 2")
        workout = db.workouts.find_one({ "_id": id });
        print("step 3", workout)
        return workout

    # def get_workout_in_db(self, name:str):
    #     workout = db.workouts.find_one({"name": name })
    #     print("workout from wokoutQur",workout)
    #     if workout:
    #         workout["id"] = str(workout["_id"])
    #         del workout["_id"]
    #         return workout
    #     else:
    #         return None

    def insert_workout(self, name, current_exercise):
        try:
            # print("current exercise:", current_exercise)
            result = db.workouts.insert_one({
                "name": name,
                "current_exercise": {
                    "bodyPart": current_exercise.bodyPart,
                    "equipment": current_exercise.equipment,
                    "gifUrl": current_exercise.gifUrl,
                    "id": current_exercise.id,
                    "name": current_exercise.name,
                    "target": current_exercise.target,
                    "sets": current_exercise.sets,
                    "reps": current_exercise.reps,
                    },
            });
            # print("result:", result)
            workout = self.get_workout(result.inserted_id)
            workout["id"] = str(workout["_id"])
            # print("insert", workout)
            del workout["_id"]
            # print("created:", workout)
            return workout
        except DuplicateKeyError:
            raise DuplicateKeyError(error)

    def update_workout(self, id, name):
        try:
            # current_exercise = dict(current_exercise)
            # print("current_exercise:", current_exercise)
            result = db.workouts.update_one(
                {"_id": id},
                [{ "$set": { "name": name}}],
            )
            if result.matched_count == 0:
                return None
            # return self.get_workout(id)
            workout = self.get_workout(id)
            return workout
        except DuplicateKeyError:
            raise DuplicateTitle()

    def update_exercise(self, id, sets, reps):
        try:
            # current_exercise = dict(current_exercise)
            # print("current_exercise:", current_exercise)
            result = db.current_exercise.update_one(
                {"id": id},
                [{ "$set": { "sets": sets}},
                { "$set": { "reps": reps}},
                ],
            )
            if result.matched_count == 0:
                return None
            # return self.get_workout(id)
            workout = self.get_workout(id)
            return workout
        except DuplicateKeyError:
            raise DuplicateTitle()
