from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
import uvicorn


from routers import (
    trainers,
    appointments,
    clients,
    Authentication,
    gym,
    time,
    date,
    workout,
)

app = FastAPI()


origins = [
    "http://localhost:3000", "http://localhost:8000", "http://localhost:8081"
    # os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#MongoDB Endpoints
app.include_router(trainers.router)
app.include_router(appointments.router)
app.include_router(clients.router)
# app.include_router(auth.router)
app.include_router(Authentication.router)
app.include_router(gym.router)
app.include_router(date.router)
app.include_router(time.router)
app.include_router(workout.router)
