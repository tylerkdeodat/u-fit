from pydantic import BaseModel
from models.Client import ClientOut
from models.trainer import TrainerOut

class AppointmentIn(BaseModel):
    time: str
    location: str
    date: str
    duration: str
    client: ClientOut
    trainer: TrainerOut
    #appointment_number: int

class AppointmentOut(AppointmentIn):
    id: str


class AppointmentList(BaseModel):
    appointments: list[AppointmentOut]

# class AppointmentDeleteOperation(BaseModel):
#     result: bool