from pydantic import BaseModel

class HttpError(BaseModel):
    detail: str


class TokenData(BaseModel):
    username: str


class AccessToken(BaseModel):
    token: str


class User(BaseModel):
    user: str
    password: str
    email: str

class UserOut(User):
    id: int
    hash_password: str
