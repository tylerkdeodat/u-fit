from pydantic import BaseModel

class ClientIn(BaseModel):
    username: str
    password: str
    name: str | None = None
    email: str | None = None
    phone_number: str | None = None
    age: str | None = None
    height: str | None = None
    current_weight: str | None = None
    goal_weight: str | None = None
    gender: str | None = None

class ClientOut(BaseModel):
    id: str
    username: str
    hashed_password: str
    name: str | None = None
    email: str | None = None
    phone_number: str | None = None
    age: str | None = None
    height: str | None = None
    current_weight: str | None = None
    goal_weight: str | None = None
    gender: str | None = None


class ClientList(BaseModel):
    clients: list[ClientOut]


class HttpError(BaseModel):
    detail: str


class TokenData(BaseModel):
    username: str


class AccessToken(BaseModel):
    token: str
