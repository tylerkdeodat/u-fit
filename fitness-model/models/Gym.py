from pydantic import BaseModel


class GymIn(BaseModel):
  name: str
  location: str


class GymOut(GymIn):
  id: str

class Gyms(BaseModel):
  gyms: list[GymOut]


class DaysOpen(BaseModel):
  monday: str
  tuesday: str
  wednesday: str
  thursday: str
  friday: str
  saturday: str
  sunday: str
  gym: GymOut

class DaysOpenOut(DaysOpen):
  id: str

class Days(BaseModel):
  dates: list[DaysOpenOut]

class HoursOpen(BaseModel):
  monday: tuple
  tuesday: tuple
  wednesday: tuple
  thursday: tuple
  friday: tuple
  saturday: tuple
  sunday: tuple
  gym:GymOut

class HoursOpenOut(HoursOpen):
  id: str

class Hours(BaseModel):
  times: list[HoursOpenOut]