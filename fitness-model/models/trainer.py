from pydantic import BaseModel
from models.Gym import GymOut

class TrainerIn(BaseModel):
    name: str
    email: str
    phone_number: str
    age: int
    gender: str
    gym: GymOut

class TrainerOut(TrainerIn):
    id: str

class Trainers(BaseModel):
   trainers: list[TrainerOut]

# class TrainerDeleteOperation(BaseModel):
#     result: bool