from site import execsitecustomize
from sys import exec_prefix
from tokenize import String
from pydantic import BaseModel

class CurrentExercise(BaseModel):
    bodyPart: str
    equipment: str
    gifUrl: str
    id: str
    name: str
    target: str
    sets: str
    reps: str

class CurrentExerciseOut(CurrentExercise):
    id: str

class WorkoutIn(BaseModel):
    name: str
    current_exercise: CurrentExercise


class WorkoutOut(WorkoutIn):
    id: str


class WorkoutList(BaseModel):
    workouts: list[WorkoutOut]

