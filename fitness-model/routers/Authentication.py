from urllib import response
from db import ClientQueries
from fastapi.responses import RedirectResponse, HTMLResponse
from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    Cookie,
    APIRouter,
    Request,
)
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
import os, sys
from passlib.context import CryptContext
from typing import Optional
from models.Authentication import HttpError, TokenData, AccessToken
from models.Client import ClientOut, ClientIn
from jose import JWTError, jwt


SIGNING_KEY = os.environ['SIGNING_KEY']
ALGORITHM = "HS256"
COOKIE_NAME = "fastapi_access_token"

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="fitness-model/token")

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    print("pass",password)
    return pwd_context.hash(password)


def authenticate_user(repo: ClientQueries, username: str, password: str):
    user = repo.get_client_in_db(username)
    print(username, password)
    if not user:
        return False
    if not verify_password(password, user["hashed_password"]):
        return False
    return user


def create_access_token(data: dict):
    to_encode = data.copy()
    encoded_jwt = jwt.encode(to_encode, SIGNING_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(
    bearer_token: Optional[str] = Depends(oauth2_scheme),
    cookie_token: Optional[str] | None = (
        Cookie(default=None, alias=COOKIE_NAME)
    ),
    repo: ClientQueries = Depends(),
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid authentication credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    token = bearer_token
    if not token and cookie_token:
        token = cookie_token
    # try:
    print(token, "  ", SIGNING_KEY, "  ", ALGORITHM)
    payload = jwt.decode(token, SIGNING_KEY, algorithms=[ALGORITHM])
    username = payload.get("sub")
    print(payload)
    if username is None:
        raise credentials_exception
    # except (JWTError, AttributeError):
    #     raise credentials_exception
    user = repo.get_client_in_db(username)
    print(user)
    if user is None:
        raise credentials_exception
    return user


@router.post("/fitness-model/token")
async def login_for_access_token(
    response: Response,
    request: Request,
    form_data: OAuth2PasswordRequestForm = Depends(),
    repo: ClientQueries = Depends(),
):
    print("trying")
    user = authenticate_user(repo, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(
        data={"sub": user},
    )
    token = {"access_token": access_token, "token_type": "bearer"}
    headers = request.headers
    samesite = "none"
    secure = True
    if "origin" in headers and "localhost" in headers["origin"]:
        samesite = "lax"
        secure = False
    response.set_cookie(
        key=COOKIE_NAME,
        value=access_token,
        httponly=True,
        samesite=samesite,
        secure=secure,
    )
    print(response)
    print("token",token)
    return token

@router.get("/fitness-model/token", response_model=AccessToken)
async def get_token(request: Request):
    if COOKIE_NAME in request.cookies:
        return {
            "token": request.cookies[COOKIE_NAME]}

# @router.get("/fitness-model/user/me", response_model=ClientIn)
# async def read_users_me(client: ClientIn = Depends(get_current_user)):
#     print("current",client)
#     return client

@router.delete("/fitness-model/token")
async def delete_cookie(request: Request, response: Response):
    if COOKIE_NAME in request.cookies:
        headers = request.headers
        samesite = "none"
        secure = True
        if "origin" in headers and "localhost" in headers["origin"]:
            samesite = "lax"
            secure = False
        response.delete_cookie(
            key=COOKIE_NAME,
            httponly=True,
            samesite=samesite,
            secure=secure,
        )
        return {"message": "Token deleted!!"}