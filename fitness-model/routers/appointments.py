from http.client import responses
from urllib import response
from fastapi import APIRouter, Response, status, Depends
from typing import Union
from bson.objectid import ObjectId
from models.Appointment import (
    AppointmentIn,
    AppointmentOut,
    AppointmentList,
)
from models.common import ErrorMessage
from models.trainer import TrainerOut
from models.Client import ClientOut
from db import AppointmentQueries, DuplicateTitle
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from typing import Optional
from pymongo import MongoClient, ASCENDING
import os


router = APIRouter()


dbhost = os.environ["MONGOHOST"]
dbname = os.environ["MONGODATABASE"]
dbuser = os.environ["MONGOUSER"]
dbpass = os.environ["MONGOPASSWORD"]
dbsecret = os.environ["SIGNING_KEY"]

connection_string = "mongodb://{}:{}@{}/{}?authSource=admin".format(dbuser, dbpass, dbhost, dbname)

client = MongoClient(connection_string)
db = client[dbname]

SIGNING_KEY = os.environ["SIGNING_KEY"]
ALGORITHM = "HS256"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token", auto_error=False)


async def get_current_user(
    token: Optional[str] = Depends(oauth2_scheme),
):
    print(token)
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid authentication credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        return jwt.decode(token, SIGNING_KEY, algorithms=[ALGORITHM])
    except (JWTError, AttributeError):
        raise credentials_exception


@router.get("/fitness-model/appointments/", response_model=AppointmentList)
def get_appointments(query=Depends(AppointmentQueries)):
    appointments = query.get_all_appointments()
    for appointment in appointments:
        appointment["id"] = str(appointment["_id"])
        return {
            "appointments": appointments,
        }

@router.get(
    "/fitness-model/appointments/{appointment_id}",
    response_model=Union[AppointmentOut, ErrorMessage],
    responses={
        200: {"model": AppointmentOut},
        404: {"model": ErrorMessage},
    },
)
def get_appointment(appointment_id: str, response: Response, query=Depends(AppointmentQueries)):
    appointment_id = ObjectId(appointment_id)
    appointment = query.get_appointment(appointment_id)
    if appointment is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Appointment not found"}
    appointment["id"] = str(appointment["_id"])
    del appointment["_id"]
    print("jerr", appointment)
    return appointment

@router.post(
    "/fitness-model/appointments/",
    # response_model=Union[TrainerOut, ErrorMessage],
    responses={
        200: {"model": AppointmentOut},
        409: {"model": ErrorMessage},
    },
)
def create_appointment(
    appointment: AppointmentIn,
    response: Response,
    query=Depends(AppointmentQueries),
):
    client = db.clients.find_one({"_id": ObjectId(appointment.client.id)})
    client["id"] = str(client["_id"])
    del client["_id"]
    trainer = db.trainers.find_one({"_id": ObjectId(appointment.trainer.id)})
    trainer["id"] = str(trainer["_id"])
    del trainer["_id"]

    try:
        appointment = query.insert_appointment(
            appointment.time,
            appointment.location,
            appointment.date,
            appointment.duration,
            client,
            trainer,
        )

        print("here",appointment)
        return appointment
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate Appointment Objects: {appointment.trainer}"}

@router.put(
    "/fitness-model/appointments/{appointment_id}",
    response_model=Union[AppointmentOut, ErrorMessage],
    responses={
        200: {"model": AppointmentOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_appointment(
    appointment_id: str,
    appointment: AppointmentIn,
    response: Response,
    query=Depends(AppointmentQueries),
):
    try:
        print("step 1")
        print(appointment)
        client = db.clients.find_one({"_id": ObjectId(appointment.client.id)})
        print(client)
        client["id"] = str(client["_id"])
        del client["_id"]

        trainer = db.trainers.find_one({"_id": ObjectId(appointment.trainer.id)})
        # print(trainer)
        trainer["id"] = str(trainer["_id"])
        del trainer["_id"]

        # gym = db.gyms.find_one({"_id": ObjectId(appointment.trainer.gym.id)})
        # print("gym",gym)
        # gym["id"] = str(gym["_id"])
        # del gym["_id"]
        appointment_id = ObjectId(appointment_id)
        appointment = query.update_appointment(
            appointment_id,
            appointment.time,
            appointment.location,
            appointment.date,
            appointment.duration,
            client,
            trainer,
        )
        print("step 5")
        # if appointment in None:
        #     response.status_code = status.HTTP_404_NOT_FOUND
        #     return {"message": "Appointment not found"}
        # print("should be right",appointment)
        appointment["id"] = str(appointment["_id"])
        del appointment["_id"]
        print(appointment)
        # print("should be right here",appointment)
        # appointment.trainer["id"] = str(appointment.trainer["_id"])
        # del appointment.trainer["_id"]
        # appointment["client"]["id"] = str(appointment.client["_id"])

        return appointment
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate Appointment Objects: {appointment.trainer}"}