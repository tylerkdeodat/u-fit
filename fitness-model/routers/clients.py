from http.client import responses
from urllib import response
from fastapi import APIRouter, Response, status, Depends
from typing import Union
from bson.objectid import ObjectId
from models.Client import (
    ClientIn,
    ClientOut,
    ClientList,
)
from models.common import ErrorMessage
from db import ClientQueries, DuplicateTitle

router = APIRouter()

@router.get("/fitness-model/clients", response_model=ClientList)
def get_clients(query=Depends(ClientQueries)):
    clients = query.get_all_clients()
    # for client in clients:
    #     client["id"] = str(client["_id"])
    return {
        "clients": clients,
    }

@router.get(
    "/fitness-model/clients/{client_id}",
    response_model=Union[ClientOut, ErrorMessage],
    responses={
        200: {"model": ClientOut},
        404: {"model": ErrorMessage},
    },
)
def get_client(client_id: str, response: Response, query=Depends(ClientQueries)):
    client_id = ObjectId(client_id)
    print(client_id)
    client = query.get_client(client_id)
    if client is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Client not found"}
    client["id"] = str(client["_id"])
    return client


@router.post(
    "/fitness-model/clients",
    response_model=Union[ClientOut, ErrorMessage],
    responses={
        200: {"model": ClientOut},
        409: {"model": ErrorMessage},
    },
)
def create_client(
    client1: ClientIn,
    response: Response,
    query=Depends(ClientQueries),
):
    print("create",client1)
    try:
        client1 = query.insert_client(
            client1.username,
            client1.password,
            client1.name,
            client1.email,
            client1.phone_number,
            client1.age,
            client1.height,
            client1.current_weight,
            client1.goal_weight,
            client1.gender,
        )
        print("Client:", client1)
        return client1
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate Client Objects: {client1.name}"}


@router.put(
    "/fitness-model/clients/{client_id}",
    response_model=Union[ClientOut, ErrorMessage],
    responses={
        200: {"model": ClientOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_client(
    client_id: str,
    client: ClientIn,
    response: Response,
    query=Depends(ClientQueries),
):
    try:
        client_id = ObjectId(client_id)
        client = query.update_client(
            client_id,
            client.username,
            client.name,
            client.email,
            client.phone_number,
            client.age,
            client.height,
            client.current_weight,
            client.goal_weight,
            client.gender,
        )
        if client is None:
            response.status_code = status.HTTP_404_NOT_FOUND
            return {"message": "Client not found"}
        client["id"] = str(client["_id"])
        return client
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate Client Objects: {client.name}"}
