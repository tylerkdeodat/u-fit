from http.client import responses
from urllib import response
from fastapi import APIRouter, Response, status, Depends
from typing import Union
from bson.objectid import ObjectId
from models.Gym import (
    DaysOpen,
    DaysOpenOut,
    Days
)
from models.common import ErrorMessage
from db import DateQueries, DuplicateTitle
from pymongo import MongoClient, ASCENDING
import os


router = APIRouter()


dbhost = os.environ["MONGOHOST"]
dbname = os.environ["MONGODATABASE"]
dbuser = os.environ["MONGOUSER"]
dbpass = os.environ["MONGOPASSWORD"]
dbsecret = os.environ["SIGNING_KEY"]

connection_string = "mongodb://{}:{}@{}/{}?authSource=admin".format(dbuser, dbpass, dbhost, dbname)

client = MongoClient(connection_string)
db = client[dbname]


@router.get("/fitness-model/dates", response_model=Days)
def get_dates(query=Depends(DateQueries)):
    dates = query.get_all_dates()
    print("dates",dates)
    for date in dates:
        date["id"] = str(date["_id"])
    return {
        "dates": dates,
    }

@router.get(
    "/fitness-model/dates/{date_id}",
    response_model=Union[DaysOpenOut, ErrorMessage],
    responses={
        200: {"model": DaysOpenOut},
        404: {"model": ErrorMessage},
    },
)
def get_date(date_id: str, response: Response, query=Depends(DateQueries)):
    date_id = ObjectId(date_id)
    print(date_id)
    date = query.get_date(date_id)
    if date is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "date not found"}
    date["id"] = str(date["_id"])
    return date


@router.post(
    "/fitness-model/dates",
    response_model=Union[DaysOpenOut, ErrorMessage],
    responses={
        200: {"model": DaysOpenOut},
        409: {"model": ErrorMessage},
    },
)
def create_date(
    date: DaysOpen,
    response: Response,
    query=Depends(DateQueries),
):
    gym = db.gyms.find_one({"_id": ObjectId(date.gym.id)})
    print("gym",gym)
    gym["id"] = str(gym["_id"])
    del gym["_id"]
    print("create",date)
    try:
        date = query.insert_date(
            date.monday,
            date.tuesday,
            date.wednesday,
            date.thursday,
            date.friday,
            date.saturday,
            date.sunday,
            gym,

        )
        print("date:", date)
        return date
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate date Objects: {date}"}

@router.put(
    "/fitness-model/dates/{date_id}",
    response_model=Union[DaysOpenOut, ErrorMessage],
    responses={
        200: {"model": DaysOpenOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_date(
    date_id: str,
    date: DaysOpen,
    response: Response,
    query=Depends(DateQueries),
):
    try:
        gym = db.gyms.find_one({"_id": ObjectId(date.gym.id)})
        print("gym",gym)
        gym["id"] = str(gym["_id"])
        del gym["_id"]
        date_id = ObjectId(date_id)
        date = query.update_date(
            date_id,
            date.monday,
            date.tuesday,
            date.wednesday,
            date.thursday,
            date.friday,
            date.saturday,
            date.sunday,
            gym
        )
        if date is None:
            response.status_code = status.HTTP_404_NOT_FOUND
            return {"message": "date not found"}
        date["id"] = str(date["_id"])
        return date
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate date Objects: {date.name}"}

