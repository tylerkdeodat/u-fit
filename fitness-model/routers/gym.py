from http.client import responses
from urllib import response
from fastapi import APIRouter, Response, status, Depends
from typing import Union
from bson.objectid import ObjectId
from models.Gym import (
    GymIn,
    GymOut,
    Gyms,
    HoursOpen,
    DaysOpen,
)
from models.common import ErrorMessage
from db import GymQueries, DuplicateTitle
import os
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from typing import Optional

router = APIRouter()

SIGNING_KEY = os.environ["SIGNING_KEY"]
ALGORITHM = "HS256"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token", auto_error=False)


async def get_current_user(
    token: Optional[str] = Depends(oauth2_scheme),
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid authentication credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        return jwt.decode(token, SIGNING_KEY, algorithms=[ALGORITHM])
    except (JWTError, AttributeError):
        raise credentials_exception



@router.get("/fitness-model/gyms", response_model=Gyms)
def get_gyms(query=Depends(GymQueries)):
    gyms = query.get_all_gyms()
    # for client in gyms:
    #     client["id"] = str(client["_id"])
    return {
        "gyms": gyms,
    }

@router.get(
    "/fitness-model/gyms/{gym_id}",
    response_model=Union[GymOut, ErrorMessage],
    responses={
        200: {"model": GymOut},
        404: {"model": ErrorMessage},
    },
)
def get_gym(gym_id: str, response: Response, query=Depends(GymQueries)):
    gym_id = ObjectId(gym_id)
    print(gym_id)
    client = query.get_gym(gym_id)
    if client is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Client not found"}
    client["id"] = str(client["_id"])
    return client


@router.post(
    "/fitness-model/gyms",
    response_model=Union[GymOut, ErrorMessage],
    responses={
        200: {"model": GymOut},
        409: {"model": ErrorMessage},
    },
)
def create_gym(
    gym: GymIn,
    response: Response,
    query=Depends(GymQueries),
):
    print("create",gym)
    try:
        gym = query.insert_gym(
            gym.name,
            gym.location,

        )
        print("Client:", gym)
        return gym
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate Client Objects: {gym.name}"}

@router.put(
    "/fitness-model/gyms/{gym_id}",
    response_model=Union[GymOut, ErrorMessage],
    responses={
        200: {"model": GymOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_gym(
    gym_id: str,
    gym: GymIn,
    response: Response,
    query=Depends(GymQueries),
):
    try:
        gym_id = ObjectId(gym_id)
        gym = query.update_gym(
            gym_id,
            gym.name,
            gym.location,
        )
        if gym is None:
            response.status_code = status.HTTP_404_NOT_FOUND
            return {"message": "gym not found"}
        gym["id"] = str(gym["_id"])
        return gym
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate gym Objects: {gym.name}"}

