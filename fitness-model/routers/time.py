from http.client import responses
from urllib import response
from fastapi import APIRouter, Response, status, Depends
from typing import Union
from bson.objectid import ObjectId
from models.Gym import (
    HoursOpen,
    HoursOpenOut,
    Hours,
)
from models.common import ErrorMessage
from db import TimeQueries, DuplicateTitle
from pymongo import MongoClient, ASCENDING
import os


router = APIRouter()


dbhost = os.environ["MONGOHOST"]
dbname = os.environ["MONGODATABASE"]
dbuser = os.environ["MONGOUSER"]
dbpass = os.environ["MONGOPASSWORD"]
dbsecret = os.environ["SIGNING_KEY"]

connection_string = "mongodb://{}:{}@{}/{}?authSource=admin".format(dbuser, dbpass, dbhost, dbname)

client = MongoClient(connection_string)
db = client[dbname]



@router.get("/fitness-model/times", response_model=Hours)
def get_times(query=Depends(TimeQueries)):
    times = query.get_all_times()
    print(times)
    for time in times:
        time["id"] = str(time["_id"])
    return {
        "times": times,
    }

@router.get(
    "/fitness-model/times/{time_id}",
    response_model=Union[HoursOpenOut, ErrorMessage],
    responses={
        200: {"model": HoursOpenOut},
        404: {"model": ErrorMessage},
    },
)
def get_time(time_id: str, response: Response, query=Depends(TimeQueries)):
    time_id = ObjectId(time_id)
    print(time_id)
    time = query.get_time(time_id)
    if time is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "time not found"}
    time["id"] = str(time["_id"])
    return time


@router.post(
    "/fitness-model/times",
    response_model=Union[HoursOpenOut, ErrorMessage],
    responses={
        200: {"model": HoursOpenOut},
        409: {"model": ErrorMessage},
    },
)
def create_time(
    time: HoursOpen,
    response: Response,
    query=Depends(TimeQueries),
):
    gym = db.gyms.find_one({"_id": ObjectId(time.gym.id)})
    print("gym",gym)
    gym["id"] = str(gym["_id"])
    del gym["_id"]
    try:
        time = query.insert_time(
            time.monday,
            time.tuesday,
            time.wednesday,
            time.thursday,
            time.friday,
            time.saturday,
            time.sunday,
            gym,

        )
        print("time:", time)
        return time
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate time Objects: {time}"}

@router.put(
    "/fitness-model/times/{time_id}",
    response_model=Union[HoursOpenOut, ErrorMessage],
    responses={
        200: {"model": HoursOpenOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_time(
    time_id: str,
    time: HoursOpen,
    response: Response,
    query=Depends(TimeQueries),
):
    try:
        gym = db.gyms.find_one({"_id": ObjectId(time.gym.id)})
        gym["id"] = str(gym["_id"])
        del gym["_id"]
        time_id = ObjectId(time_id)
        time = query.update_time(
            time_id,
            time.monday,
            time.tuesday,
            time.wednesday,
            time.thursday,
            time.friday,
            time.saturday,
            time.sunday,
            gym
        )
        if time is None:
            response.status_code = status.HTTP_404_NOT_FOUND
            return {"message": "time not found"}
        time["id"] = str(time["_id"])
        return time
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate time Objects: {time.name}"}

