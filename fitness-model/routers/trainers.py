from http.client import responses
from urllib import response
from pymongo import MongoClient, ASCENDING
from fastapi import APIRouter, Response, status, Depends
from typing import Union
from bson.objectid import ObjectId
from models.trainer import (
    TrainerIn,
    TrainerOut,
    Trainers,
)
from models.common import ErrorMessage
import os
from db import TrainerQueries, DuplicateTitle

router = APIRouter()

dbhost = os.environ["MONGOHOST"]
dbname = os.environ["MONGODATABASE"]
dbuser = os.environ["MONGOUSER"]
dbpass = os.environ["MONGOPASSWORD"]
dbsecret = os.environ["SIGNING_KEY"]

connection_string = "mongodb://{}:{}@{}/{}?authSource=admin".format(dbuser, dbpass, dbhost, dbname)

client = MongoClient(connection_string)
db = client[dbname]


@router.get("/fitness-model/trainers", response_model=Trainers)
def get_trainers(query=Depends(TrainerQueries)):
    print("step 1")
    trainers = query.get_all_trainers()
    for trainer in trainers:
        trainer["id"] = str(trainer["_id"])
        del trainer["_id"]
    print("step 3")
    print("trainers:", trainers)
    return {
        "trainers": trainers,
    }


@router.get(
    "/fitness-model/trainers{trainer_id}",
    response_model=Union[TrainerOut, ErrorMessage],
    responses={
        200: {"model": TrainerOut},
        404: {"model": ErrorMessage},
    },
)
def get_trainer(trainer_id: str, response: Response, query=Depends(TrainerQueries)):
    trainer_id = ObjectId(trainer_id)
    trainer = query.get_trainer(trainer_id)
    if trainer is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Trainer not found"}
    trainer["id"] = str(trainer["_id"])
    return trainer



@router.post(
    "/fitness-model/trainers",
    response_model=Union[TrainerOut, ErrorMessage],
    responses={
        200: {"model": TrainerOut},
        409: {"model": ErrorMessage},
    },
)
def create_trainer(
    trainer: TrainerIn,
    response: Response,
    query=Depends(TrainerQueries),
):
    gym = db.gyms.find_one({"_id": ObjectId(trainer.gym.id)})
    print("gym",gym)
    gym["id"] = str(gym["_id"])
    del gym["_id"]
    try:
        trainer = query.insert_trainer(
            trainer.name,
            trainer.email,
            trainer.phone_number,
            trainer.age,
            trainer.gender,
            gym
        )
        return trainer
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate Trainer Objects: {trainer.name}"}


@router.put(
    "/fitness-model/trainers/{trainer_id}",
    response_model=Union[TrainerOut, ErrorMessage],
    responses={
        200: {"model": TrainerOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_trainer(
    trainer_id: str,
    trainer: TrainerIn,
    response: Response,
    query=Depends(TrainerQueries),
):
    try:
        trainer_id = ObjectId(trainer_id)
        trainer = query.update_trainer(
            trainer_id,
            trainer.name,
            trainer.email,
            trainer.phone_number,
            trainer.age,
            trainer.gender,
        )
        if trainer is None:
            response.status_code = status.HTTP_404_NOT_FOUND
            return {"message": "Trainer not found"}
        trainer["id"] = str(trainer["_id"])
        return trainer
    except DuplicateTitle:
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": f"Duplicate Trainer Objects: {trainer.name}"}


# @router.delete(
#     "/fitness-model/trainers/{trainer_id}",
#     response_model=TrainerDeleteOperation,
# )
# def delete_trainer(trainer_id: str, query=Depends(TrainerQueries)):
#     try:
#         trainer_id = ObjectId(trainer_id)
#         query.delete_trainer(trainer_id)
#         return {"result": True}
#     except:
#         return {"result": False}
