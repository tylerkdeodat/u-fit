from http.client import responses
from urllib import response
from fastapi import APIRouter, Response, status, Depends
from typing import Union
from bson.objectid import ObjectId
from models.workout import (
    WorkoutIn,
    WorkoutOut,
    WorkoutList,
    CurrentExercise,
)
from models.common import ErrorMessage
from db import WorkoutQueries

router = APIRouter()


@router.get(
    "/fitness-model/workouts/", 
    response_model=WorkoutList,
)
def get_all_workouts(query=Depends(WorkoutQueries)):
    print("step 1")
    workouts = query.get_all_workouts()
    for workout in workouts:
        workout["id"] = str(workout["_id"])
        del workout["_id"]
    print("step 3")
    print("step 4:", workouts)
    return {
        "workouts": workouts,
    }

@router.get(
    "/fitness-model/workouts/{workout_id}",
    response_model=Union[WorkoutOut, ErrorMessage],
    responses={
        200: {"model": WorkoutOut},
        404: {"model": ErrorMessage},
    },
)
def workout_details(workout_id: str, response: Response, query=Depends(WorkoutQueries)):
    print("step 1")
    workout_id = ObjectId(workout_id)
    workout = query.get_workout(workout_id)
    if workout is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Workout not found"}
    workout["id"] = str(workout["_id"])
    del workout["_id"]
    print("step 4", workout)
    return workout



@router.post(
    "/fitness-model/workouts",
    # response_model=Union[WorkoutOut, ErrorMessage],
    responses={
        200: {"model": WorkoutOut},
        409: {"model": ErrorMessage},
    },
)
def create_workout(
    workout: WorkoutIn,
    # response: Response,
    query=Depends(WorkoutQueries),
):
    workout = query.insert_workout(
        workout.name,
        workout.current_exercise,
    )
    # print("/n/n/n", workout, "/n/n/n")
    return workout

@router.put(
    "/fitness-model/workouts/{workout_id}",
    response_model=Union[WorkoutOut, ErrorMessage],
    responses={
        200: {"model": WorkoutOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_workout(
    workout_id: str,
    workout: WorkoutIn,
    response: Response,
    query=Depends(WorkoutQueries),
):
    workout_id = ObjectId(workout_id)
    workout = query.update_workout(
        workout_id,
        workout.name,
    )
    if workout is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Workout not found"}
    workout["id"] = str(workout["_id"])
    del workout["_id"]
    return workout

@router.put(
    "/fitness-model/workouts/{workout_id}",
    response_model=Union[WorkoutOut, ErrorMessage],
    responses={
        200: {"model": WorkoutOut},
        404: {"model": ErrorMessage},
        409: {"model": ErrorMessage},
    },
)
def update_exercise(
    exercise_id: str,
    exercise: CurrentExercise,
    response: Response,
    query=Depends(WorkoutQueries),
):
    exercise_id = CurrentExercise.id
    exercise = query.update_exercise(
        exercise_id,
        exercise.sets,
        exercise.reps,
    )
    if exercise is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Exercise not found"}
    # workout["id"] = str(workout["_id"])
    # del workout["_id"]
    return exercise

