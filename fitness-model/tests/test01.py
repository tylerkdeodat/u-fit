from subprocess import call
from fastapi.testclient import TestClient
from unittest import TestCase
from db import WorkoutQueries
from db import GymQueries

# class WorkoutQueriesInfo(TestCase):
#     def search_workout():


def test_account_queries_exist():
    from db import AccountsQueries

def test_account_queries_has_get_user():
    from db import AccountsQueries
    user = AccountsQueries()
    assert callable(user.get_user)

def test_gym_queries_has_get_gym():
    from db import GymQueries
    gym = GymQueries()
    assert callable(gym.get_gym)


# class AccountInfo(TestCase):
#     def update_account(
#         self, id, username, email, password, first_name, last_name
#     ):
#         return [10, "test", "test", "test", "test"]


# async def override_get_fake_user():
#     return {
#         "id": 1,
#         "username": "TailsPrower",
#         "email": "Tails@Sonic.com",
#         "full_name": "Tails Prower",
#     }


# async def override_update_account():
#     return {
#         "id": 1,
#         "username": "TailsPrower",
#         "email": "TailsheartSonic@Sonic.com",
#         "first_name": "Tails",
#         "last_name": "Sonic",
#     }


# app.dependency_overrides[update_account] = override_update_account
# app.dependency_overrides[get_current_user] = override_get_fake_user

# client = TestClient(app)


# def test_update_account():
#     app.dependency_overrides[ProfileQueries] = AccountInfo
#     app.dependency_overrides[get_current_user] = override_get_fake_user
#     r = client.put(
#         "/api/accounts/myself",
#         json={
#             "username": "string",
#             "email": "string",
#             "password": "string",
#             "first_name": "string",
#             "last_name": "string",
#         },
#     )
#     d = r.json()
#     print("today", d)
#     assert r.status_code == 200

#     app.dependency_overrides = {}
