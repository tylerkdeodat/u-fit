from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app
from unittest import TestCase
from db import AppointmentQueries


app = FastAPI()
async def read_main():
    return {"msg": "Hello World"}


client = TestClient(app)


def test_appointment_queries_exist():
    from db import AppointmentQueries

def test_appointment_queries_has_get_appointment():
    from db import AppointmentQueries
    user = AppointmentQueries()
    assert callable(user.get_appointment)

class EmptyCategoryQueries:
    def get_category(self, id):
        return None

class NormalCategoryQueries:
    def get_category(self, id):
        return [id, "OUR APPOINTMENT QUERIES", True]

def test_get_category_returns_404():
    # ARRANGE
    # Use our fake database
    app.dependency_overrides[AppointmentQueries] = EmptyCategoryQueries

    # ACT
    # Make the request
    response = client.get("/api/postgres/categories/1")

    # ASSERT
    # Assert that we got a 404
    assert response.status_code == 404

    # CLEAN UP
    # Clear out the dependencies
    app.dependency_overrides = {}
