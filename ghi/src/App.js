import { BrowserRouter, Routes, Route } from 'react-router-dom';

import './App.css';
import SignUp from "./SignUp";
import ClientForm from './ClientForm';
import WorkoutForm from './workoutForm';
import MainPage from './MainPage';
import Login from "./Login";
import Logout from "./Logout";
import React from 'react';
import SearchedExercises from './pages/SearchedExercises';
import ExerciseDetail from './pages/ExerciseDetail';

import Navbar from './components/Navbar';
import { useAuthContext, useToken } from './Auth';


function App(){
  const [ token ]= useToken()
  const { decoded } = useAuthContext();
  console.log(token, decoded);

  return (
    <>
    <BrowserRouter>
    <Navbar />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/exercises" element={<SearchedExercises />} />
          <Route path='/exercise'>
            <Route path=":id" element={<ExerciseDetail />} />
          </Route>
          <Route path="/clients">
              <Route path="new" element={<ClientForm />} />
          </Route>
          <Route path="/workouts" element={<WorkoutForm />} />
        </Routes>
    </BrowserRouter>
    </>
  )
}

export default App;







