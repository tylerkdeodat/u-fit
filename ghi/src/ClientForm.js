// import { AuthContext } from './Auth';

import React from 'react';

class ClientForm extends React.Component {
  // static contextType = AuthContext;
  constructor(props) {
      super(props)
      this.state = {
          name: '',
          email: '',
          phone_number: '',
          age: '',
          height: '',
          current_weight: '',
          goal_weight: '',
          gender: '',
      };
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleEmailChange = this.handleEmailChange.bind(this);
      this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
      this.handleAgeChange = this.handleAgeChange.bind(this);
      this.handleHeightChange = this.handleHeightChange.bind(this);
      this.handleCurrentWeightChange = this.handleCurrentWeightChange.bind(this);
      this.handleGoalWeightChange = this.handleGoalWeightChange.bind(this);
      this.handleGenderChange = this.handleGenderChange.bind(this);
  }

  async handleSubmit(event) {
    // const token = this.context.token;
      event.preventDefault();
      const data = {...this.state};

      const clientUrl = 'http://localhost:8000/fitness-model/clients/';
      const request = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            // Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
        },
      };
      const response = await fetch(clientUrl, request);
      if (response.ok) {
          const newClient = await response.json();
          console.log(newClient);

          const cleared = {
              name: '',
              email: '',
              phone_number: '',
              age: '',
              height: '',
              current_weight: '',
              goal_weight: '',
              gender: '',
          };
          this.setState(cleared);
      }
  }

  handleNameChange(event) {
      const value = event.target.value;
      this.setState({ name: value });
  }

  handleEmailChange(event) {
      const value = event.target.value;
      this.setState({ email: value });
  }

  handlePhoneNumberChange(event) {
      const value = event.target.value;
      this.setState({ phone_number: value });
  }

  handleAgeChange(event) {
      const value = event.target.value;
      this.setState({ age: value });
  }
  
  handleHeightChange(event) {
      const value = event.target.value;
      this.setState({ height: value });
  }

  handleCurrentWeightChange(event) {
      const value = event.target.value;
      this.setState({ current_weight: value });
  }

  handleGoalWeightChange(event) {
      const value = event.target.value;
      this.setState({ goal_weight: value });
  }

  handleGenderChange(event) {
      const value = event.target.value;
      this.setState({ gender: value });
  }

  render() {
      return (
        <>
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>First let's get some info</h1>
                <form onSubmit={this.handleSubmit} id="create-client-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmailChange} value={this.state.email} placeholder="Email" required type="text" name="email" id="email" className="form-control" />
                    <label htmlFor="email">Email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePhoneNumberChange} value={this.state.phone_number} placeholder="Phone Number" required type="number" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="phone_number">Phone Number</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleAgeChange} value={this.state.age} placeholder="Age" required type="text" name="age" id="age" className="form-control" />
                    <label htmlFor="age">Age</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleHeightChange} value={this.state.height} placeholder="height" required type="text" name="height" id="height" className="form-control" />
                    <label htmlFor="height">height</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleCurrentWeightChange} value={this.state.current_weight} placeholder="Current Weight" required type="text" name="current_weight" id="current_weight" className="form-control" />
                    <label htmlFor="current_weight">Current Weight</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleGoalWeightChange} value={this.state.goal_weight} placeholder="Goal Weight" required type="text" name="goal_weight" id="goal_weight" className="form-control" />
                    <label htmlFor="goal_weight">Goal Weight</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleGenderChange} value={this.state.gender} placeholder="Gender" required type="text" name="gender" id="gender" className="form-control" />
                    <label htmlFor="gender">Gender</label>
                  </div>
                  <button className="btn btn-outline-dark">Create</button>
                </form>
              </div>
            </div>
          </div>
        </>
      )
  }
}

export default ClientForm;