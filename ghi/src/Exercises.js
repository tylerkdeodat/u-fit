import React, { useEffect, useState} from "react";
import { AuthContext, useAuthContext } from './Auth';


const REACT_APP_EXERCISE_API_KEY = process.env.REACT_APP_EXERCISE_API_KEY
export default function ExerciseList() {
  const [clicked, setClicked] = useState(false)
  const [exercises, setExercises] = useState([])
  const [searchInput, setSearchInput] = useState('');
  const [filteredResults, setFilteredResults] = useState([]);
  const [current_exercise, setCurrentExercise] = useState({
    'bodyPart': '',
    'equipment': '',
    'gifUrl': '',
    'id': '',
    'name': '',
    'target': '',
  });
  const [workoutData, setWorkoutData] = useState({
    'name': '',
    'reps': '',
    'sets': '',
  });

  const { token } = useAuthContext()
  async function loadExercises() {
    // const { token } = useAuthContext();
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': REACT_APP_EXERCISE_API_KEY,
        'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com'
      }
    };

    fetch('https://exercisedb.p.rapidapi.com/exercises', options)
      .then(response => response.json())
      .then(response => setExercises(response))
      .catch(err => console.error(err));
  }
  useEffect(() => {
   loadExercises()
  }, [])
  const handleChange = event => {
    setSearchInput(event.target.value);
    const searchInput = event.target.value
    if (searchInput !== '') {
        const filteredData = exercises.filter((item) => {
          return Object.values(item).join(' ').toLowerCase().includes(searchInput.toLowerCase())
      })
      setFilteredResults(filteredData)
      } else{
          setFilteredResults([])
      }
  };

  const handleClick = async (event) => {
    console.log(event.target.value)
    setClicked(true)
    // console.log(id)
    // event.preventDefault();
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': REACT_APP_EXERCISE_API_KEY,
        'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com'
      }
    };
    fetch(`https://exercisedb.p.rapidapi.com/exercises/exercise/${event.target.value}`, options)
      .then(response => response.json())
      .then(response => setCurrentExercise({
        'bodyPart': response.bodyPart,
        'equipment': response.equipment,
        'gifUrl': response.gifUrl,
        'id': response.id,
        'name': response.name,
        'target': response.target,
      }))
      .catch(err => console.error(err));
      console.log(current_exercise)
      const request = {
        method: 'POST',
        body: JSON.stringify({...workoutData, current_exercise}),
        headers: {
          // Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
      fetch('http://localhost:8000/fitness-model/workouts', request)
        .then(response => response.json())
        .then(response => setCurrentExercise({
          'bodyPart': response.bodyPart,
          'equipment': response.equipment,
          'gifUrl': response.gifUrl,
          'id': response.id,
          'name': response.name,
          'target': response.target,
        }))
        console.log({...workoutData, current_exercise});

  };


  return (
    <>

    {clicked ? (
      <div className="row">
      <h1>Choose Your Exercise</h1>
      <input type="text" placeholder="Search" value={searchInput} onChange={handleChange}/>
      <table className="table table-striped" key = "table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Body part</th>
              <th>Target muscle</th>
              <th>Equipment</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
          {filteredResults.map(exercise => {
            return (
              <tr  key={exercise.id}>
                <td>{ exercise.name }</td>
                <td>{ exercise.bodyPart }</td>
                <td>{ exercise.target }</td>
                <td>{ exercise.equipment }</td>
                <td><img src={ exercise.gifUrl } alt="loading..."/></td>
                <td>
                  <button value={exercise.id} onClick={handleClick} className="btn btn-primary">Add Exercise</button>
                </td>
              </tr>
              );
            })}
          </tbody>
      </table>
    </div>
    ) : (
    <>
    <div className="row">
      <input type="text" placeholder="Search" value={searchInput} onChange={handleChange}/>
      <table className="table table-striped" key = "table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Body part</th>
              <th>Target muscle</th>
              <th>Equipment</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
          {filteredResults.map(exercise => {
            return (
              <tr  key={exercise.id}>
                <td>{ exercise.name }</td>
                <td>{ exercise.bodyPart }</td>
                <td>{ exercise.target }</td>
                <td>{ exercise.equipment }</td>
                <td><img src={ exercise.gifUrl } alt="loading..."/></td>
                <td>
                  <button value={exercise.id} onClick={handleClick} className="btn btn-primary">Would you like to save?</button>
                </td>
              </tr>
              );
            })}
          </tbody>
      </table>
    </div>
    </>
    )}
  </>
  );
}