import { useState, useEffect } from "react";
import { useNavigate, NavLink } from "react-router-dom";
import { useToken } from "./Auth";

import LoginInfo from './LoginInfo';

const Login = () => {
  const [, login] = useToken()
  const navigate = useNavigate();


  const [formData, setFormData] = useState({
    username: "",
    password: "",

  })
  const [success, setSuccess] = useState(false);


  const handleSubmit = async (e) => {
    e.preventDefault();
    await login(formData.username, formData.password)
    setSuccess(true)
    navigate("/");
    }

  const[page, setPage] = useState(0);


  const PageDisplay = () => {
    if (page === 0) {
      return <LoginInfo formData ={formData} setFormData={setFormData}/>
      }
  };

  return (
    <>

      {success ? (
        <section>
          <h1>You are logged in</h1>
          <br />
          <p>
            <a href="/">Home</a>
          </p>
        </section>
      ) : (
        <section>
          <div>


          <div className='items-center h-screen w-screen bg-gradient-to-bl bg-[#eeb359] from-[#f5c57c] py-[140px]'>
          <div className='flex flex-col justify-center'>
              <form className='max-w-[400px] w-full mx-auto bg-gray-200 p-8 px-8 rounded-lg shadow-xl' onSubmit={handleSubmit}>
                  <h2 className='text-3xl text-black uppercase font-semibold text-center'>Sign In</h2>
                  <div className='flex flex-col text-gray-900 py-2'>
                    {PageDisplay()}
                  </div>

                  <div className='flex justify-between item-center'>
                    <button className='w-full py-2 bg-green-500 rounded-xl font-bold uppercase hover:bg-green-400 shadow-sm text-white'>Sign In</button>
                  </div>
                  <div>
                  <NavLink to="/signup">Don't have an account?
                <button className="w-full py-2 bg-green-500 rounded-xl font-bold uppercase hover:bg-green-400 shadow-sm text-white">
                  {" "}
                  Sign Up
                </button>
              </NavLink>
                  </div>
              </form>
          </div>
      </div>
          {/* <form onSubmit={handleSubmit}>
            <label htmlFor="username">Username:</label>
            <input
              type="text"
              id="username"
              autoComplete="off"
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              required
            />
            <label htmlFor="password">Password:</label>
            <input
              type="password"
              id="password"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              required
            />
            <button>Login</button>
          </form> */}
          
          </div>
        </section>
      )}
    </>
  )
}

export default Login
