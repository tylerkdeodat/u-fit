import { useState } from "react";
import { useToken } from "./Auth";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "./Auth";


const Logout = () => {
  const [, , logout] = useToken()
  const {token, setToken} = useAuthContext
  const navigate = useNavigate();
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    await logout()
    setSuccess(true)
    navigate("/")
    }

  return (
    <>

    {success ? (
      <section>
        
        </section>
    ) : (
   <section>

    <div className='items-center h-screen w-screen bg-gradient-to-bl bg-[#eeb359] from-[#f5c57c] py-[140px]'>
    <div className='flex flex-col justify-center'>
              <form className='max-w-[400px] w-full mx-auto bg-gray-200 p-8 px-8 rounded-lg shadow-xl' onSubmit={handleSubmit}>
                  <h2 className='text-3xl text-black uppercase font-semibold text-center'>Logout</h2>
                  <div className='flex flex-col text-gray-900 py-2'>
                  </div>

                  <div className='flex justify-between item-center'>
                    <button className='w-full py-2 bg-green-500 rounded-xl font-bold uppercase hover:bg-green-400 shadow-sm text-white'>Yall come back now!</button>
                  </div>
              </form>
          </div>
        </div>
   </section>
    )}
   </>
  )
}

export default Logout

