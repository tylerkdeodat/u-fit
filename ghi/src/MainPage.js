import Hero from './components/Hero';
import About from './components/About';
import Training from './components/Training';
import Workouts from './components/Workouts';
import Pricing from './components/Pricing';
import Footer from './components/Footer';

function MainPage() {
    return (
      <>

      <Hero />
      <About />
      <Training />
      <Workouts />
      <Pricing />
      <Footer />

    </>
    );
  }

  export default MainPage;



//   <div className='px-4 py-5 my-5 text-center'>
//   <h1 className='display-5 fw-bold text-green'>Here to PUMP YOU UP!</h1>
//   <div className='col-lg-6 mx-auto'>
//     <p className='lead mb-4'>
//       {/* text for our front page */}
//     </p>
//     <div>
//       <img
//         src='https://media1.giphy.com/media/xUPGcKoAYCn5fHK0Zq/giphy.gif?cid=ecf05e47ltjqk7bgsy7em5nwjdealsw45vngftywytvemljs&rid=giphy.gif&ct=g'
//         alt='Spencer did a thing'
//         height='400'
//       />
//     </div>
//   </div>
// </div>