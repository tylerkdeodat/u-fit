import React, { useState} from 'react'
import SignUpInfo from './SignUpInfo';
import { useToken } from "./Auth";




const SignUp = () => {
  const [, , , signup] = useToken()
  const [formData, setFormData] = useState({
    username: "",
    password: "",

  })

  const handleSubmit = async (event) => {
    event.preventDefault();
    signup(formData.username, formData.password)

  }


  const[page, setPage] = useState(0);

  const FormTitles = ["Sign Up", "Your Information", "Location", "Tell us a little more about you!"];

  const PageDisplay = () => {
    if (page === 0) {
      return <SignUpInfo formData ={formData} setFormData={setFormData}/>
      }
  };


  return (
    <>

      <div className='items-center h-screen w-screen bg-gradient-to-bl bg-[#eeb359] from-[#f5c57c] py-[140px]'>
          <div className='flex flex-col justify-center'>
              <form className='max-w-[400px] w-full mx-auto bg-gray-200 p-8 px-8 rounded-lg shadow-xl' onSubmit={handleSubmit}>
                  <h2 className='text-3xl text-black uppercase font-semibold text-center'>{FormTitles[page]}</h2>
                  <div className='flex flex-col text-gray-900 py-2'>
                    {PageDisplay()}
                  </div>

                  <div className='flex justify-between item-center'>
                    <button className='w-full py-2 bg-green-500 rounded-xl font-bold uppercase hover:bg-green-400 shadow-sm text-white'>Create</button>
                  </div>
              </form>
          </div>
      </div>
    </>
  )
}

export default SignUp

