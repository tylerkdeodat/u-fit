import { useState } from 'react';

function BootstrapInput(props) {
    const {id, placeholder, labelText, value, onChange, type} = props;

return (
    <div className="mb-4">
        <label htmlFor={id} className="form-label">{labelText}</label>
        <input value={value} onChange={onChange} required type={type} className="form-control" id={id} placeholder={placeholder} />
    </div>
)
}


function TrainerForm(props) {
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');

    return (
        <form>
            <BootstrapInput 
                id="email" 
                placeholder="you@example.com" 
                labelText="Your Email Adress"
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="email" />
            <BootstrapInput 
                id="name" 
                placeholder="you@example.com" 
                labelText="Your Name"
                value={name}
                onChange={e => setName(e.target.value)}
                type="email" />
            <BootstrapInput 
                id="password" 
                placeholder="Your Super Duper Secret Password" 
                labelText="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                type="email" />
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
}

export default TrainerForm