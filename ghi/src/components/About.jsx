import React from 'react'

const About = () => {
  return (
    <div name='About' className='w-full my-32'>
        <div className='max-w-[1240px] mx-auto'>
            <div className='text-center'>
                <h2 className='text-5xl font-bold'>Trusted by Trainers across the WORLD!</h2>
                <p className='text-3xl py-6 text-gray-500'>Achieve physical, emotional and spiritual well-being while strengthening your body and mind with weight training, cardiovascular exercises, and many other options to reach your goal!  Our community is here to provide you with an effective, inspiring workout.  Learn new moves and expand your workout repertoire as you learn new moves to shake up your routine.</p>
            </div>
            <div className='grid md:grid-cols-3 gap-1 px-2 text-center'>
                <div className='border py-8 rounded-xl shadow-xl'>
                    <p className='text-6xl font-bold text-indigo-600'>100%</p>
                    <p className='text-gray-400 mt-2'>Satisfaction</p>
                </div>
                <div className='border py-8 rounded-xl shadow-xl'>
                    <p className='text-6xl font-bold text-indigo-600'>24/7</p>
                    <p className='text-gray-400 mt-2'>Assistance</p>
                </div>
                <div className='border py-8 rounded-xl shadow-xl'>
                    <p className='text-6xl font-bold text-indigo-600'>100K+</p>
                    <p className='text-gray-400 mt-2'>Clients and Trainers</p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default About