import React from 'react'

import {
    FaFacebook,
    FaGithub,
    FaInstagram,
    FaTwitch,
    FaTwitter,
}from 'react-icons/fa'

const Footer = () => {
  return (
    <div className='w-full mt-24 bg-slate-900 text-gray-300 py-y px-2'>
        <div className='max-w-[1240px] mx-auto grid grid-cols-2 md:grid-cols-6 border-b-2 border-gray-600 py-8'>
            <div>
                <h6 className='font-bold uppercase pt-2'>HOME</h6>
                <ul>
                    <li className='py-1'>GNH.com</li>
                    <li className='py-1'>Blog</li>
                    <li className='py-1'>Shop Gear</li>
                    <li className='py-1'>Member Portal</li>
                </ul>
            </div>
            <div>
                <h6 className='font-bold uppercase pt-2'>GET STARTED</h6>
                <ul>
                    <li className='py-1'>Join Today!</li>
                    <li className='py-1'>Training Info</li>
                    <li className='py-1'>Contact Info</li>
                </ul>
            </div>
            <div>
                <h6 className='font-bold uppercase pt-2'>COMPANY</h6>
                <ul>
                    <li className='py-1'>About</li>
                    <li className='py-1'>Careers</li>
                    <li className='py-1'>Diversity, Equity & Inclusion</li>
                    <li className='py-1'>Partners</li>
                </ul>
            </div>
            <div>
                <h6 className='font-bold uppercase pt-2'>Legal</h6>
                <ul>
                    <li className='py-1'>Privacy Policy</li>
                    <li className='py-1'>Terms & Conditions</li>
                    <li className='py-1'>Wavier</li>
                </ul>
            </div>
            <div className='col-span-2 pt-8 md:pt-2'>
                <p className='font-bold uppercase'>Subscribe to our newsletter</p>
                <p className='py-4'>The latest news, articles, and resources, sent to your inbox weekly!</p>
                <form className='flex flex-col sm:flex-row'>
                    <input className='w-full p-2 mr-4 rounded-md mb-4' type='email' placeholder='Enter your email'/>
                    <button className='p-2 mb-4'>Subscribe</button>
                </form>
            </div>
        </div>
        <div className='flex flex-col max-w-[1240px] px-2 py-4 mx-auto justify-between sm:flex-row text-center text-gray-500'>
            <p className='py-4'>2022 Back-End-Boys, LLC. All rights reserved</p>
            <div className='flex justify-between sm:w-[300px] pt-4 text-2xl'>
                <FaFacebook />
                <FaInstagram />
                <FaTwitter />
                <FaGithub />
                <FaTwitch />

            </div>
        </div>
    </div>
  )
}

export default Footer