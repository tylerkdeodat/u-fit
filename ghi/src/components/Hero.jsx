import React from 'react'
import { FaDumbbell, FaRunning } from "react-icons/fa";
import { IoIosPeople } from "react-icons/io";
import { GiMuscleUp } from "react-icons/gi";
import bgImg from '../assets/gymimage01.webp';
import { Link as RouterLink } from "react-router-dom";

const Hero = () => {
  return (
    <div name='Home' className='w-screen h-screen bg-orange-200 flex flex-col justify-between' >
        <div className='grid md:grid-cols-2 max-w-[1240px] m-auto'>
            <div className='flex flex-col justify-center md:items-start w-full px-2 py-8'>
                <p className='text-2xl'>Training</p>
                <h1 className='py-3 text-5xl md:text-7xl font-bold'>Exercises</h1>
                <p className='text-2xl'>This is our Company</p>
                <RouterLink to='/signup' className='w-full'><button className='py-3 px-6 sm:w-[60%] text-xl my-4 font-extrabold bg-gradient-to-r from-green-500 to-violet-300 border border-transparent'>Get Started!</button></RouterLink>
            </div>
            <div>
                <img className='w-full' src={bgImg} alt="/" />
            </div>
            <div className='absolute flex flex-col py-8 md:min-w-[760px] bottom-[5%] mx-1 md:left-1/2 transform md:-translate-x-1/2 bg-blue-100 border border-slate-300 rounded-xl text-center shadow-xl'>
                <p className='text-xl font-bold'>Find Your Motivation!</p>
                <div className='flex justify-between flex-wrap px-12'>
                    <p className='flex px-4 py-2 text-slate-500'><FaDumbbell className='h-4 text-indigo-600 m-1 text-xl'/>  Goals</p>
                    <p className='flex px-4 py-2 text-slate-500'><FaRunning className='h-5 text-indigo-600 mr-1'/>  Commitment</p>
                    <p className='flex px-4 py-2 text-slate-500'><IoIosPeople className='h-6 text-indigo-600 mr-1 text-2xl'/>  Community</p>
                    <p className='flex px-4 py-2 text-slate-500'><GiMuscleUp className='h-6 text-indigo-600 mr-1 text-xl'/>  Achievement</p>
                </div>
            </div>
        </div>

    </div>
  )
}

export default Hero



