import React, { useState } from "react";
import { Link } from "react-scroll";
import { Link as RouterLink, useLocation, NavLink } from "react-router-dom";
import { FcSearch } from "react-icons/fc";
import { useAuthContext } from "../Auth";

import { MenuIcon, XIcon } from "@heroicons/react/outline";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  const handleClick = () => setNav(!nav);

  const handleClose = () => setNav(!nav);

  const { token } = useAuthContext();

  let location = useLocation();

  return (
    <div className="w-screen h-[80px] z-10 bg-blue-100 drop-shadow-lg">
      <div className="px-2 flex justify-between items-center w-screen h-full">
        <div className="flex items-center">
          <h1 className="text-3xl font-bold mr-4 sm:text-4xl bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-violet-500">
            GNH
          </h1>
          <ul className="hidden md:flex font-sans">
            {/* conditional expression, ternary operator */}
            {location.pathname !== "/" ? (
              <li>
                <RouterLink to={"/"}>Home</RouterLink>
              </li>
            ) : (
              <li>
                <Link to="Home" smooth={true} duration={500}>
                  Home
                </Link>
              </li>
            )}
            {location.pathname !== "/" ? (
              <li>
                <RouterLink to={"/"}>About</RouterLink>
              </li>
            ) : (
              <li>
                <Link to="About" smooth={true} offset={-200} duration={500}>
                  About
                </Link>
              </li>
            )}
            {location.pathname !== "/" ? (
              <li>
                <RouterLink to={"/"}>Training</RouterLink>
              </li>
            ) : (
              <li>
                <Link to="Training" smooth={true} offset={-50} duration={500}>
                  Training
                </Link>
              </li>
            )}
            {location.pathname !== "/" ? (
              <li>
                <RouterLink to={"/"}>Workouts</RouterLink>
              </li>
            ) : (
              <li>
                <Link to="Workouts" smooth={true} offset={-100} duration={500}>
                  Workouts
                </Link>
              </li>
            )}
            {location.pathname !== "/" ? (
              <li>
                <RouterLink to={"/"}>Pricing</RouterLink>
              </li>
            ) : (
              <li>
                <Link to="Pricing" smooth={true} offset={-25} duration={500}>
                  Pricing
                </Link>
              </li>
            )}
            <li>
              <RouterLink to="/exercises" className="pr-1">
                Exercise
                <FcSearch className="text-xl float-right" />
              </RouterLink>
            </li>
          </ul>
        </div>
        <div className="hidden md:flex pr-4">
          {token ?  (
            <ul className="nav-item">
              <NavLink to="/logout">
                <button className="border-none bg-transparent text-red-800 font-extrabold mr-4 py-3">
                  {" "}
                  Rage Quit
                </button>
              </NavLink>
            </ul>
          ) : (
            <>
              <ul className="nav-item">
                <NavLink to="/login">
                  <button className="border-none bg-transparent text-black mr-4 py-3">
                    {" "}
                    Sign In
                  </button>
                </NavLink>
              </ul>
              <RouterLink to="/signup">
                {" "}
                <button className="px-8 py-3 "> Sign Up</button>
              </RouterLink>
            </>
          )}
        </div>
        <div className="md:hidden mr-4" onClick={handleClick}>
          {!nav ? <MenuIcon className="w-5" /> : <XIcon className="w-5" />}
          {/* if nav is opposite is true then execute this */}
        </div>
      </div>

      <ul className={!nav ? "hidden" : "absolute bg-zinc-200 w-full px-8"}>
        <li className="border-b-2 border-zinc-300 w-full">
          <Link onClick={handleClose} to="Home" smooth={true} duration={500}>
            Home
          </Link>
        </li>
        <li className="border-b-2 border-zinc-300 w-full">
          <Link
            onClick={handleClose}
            to="About"
            smooth={true}
            offset={-200}
            duration={500}
          >
            About
          </Link>
        </li>
        <li className="border-b-2 border-zinc-300 w-full">
          <Link
            onClick={handleClose}
            to="Trainer"
            smooth={true}
            offset={-50}
            duration={500}
          >
            Training
          </Link>
        </li>
        <li className="border-b-2 border-zinc-300 w-full">
          <Link
            onClick={handleClose}
            to="Workouts"
            smooth={true}
            offset={-100}
            duration={500}
          >
            Workouts
          </Link>
        </li>
        <li className="border-b-2 border-zinc-300 w-full">
          <Link
            onClick={handleClose}
            to="Pricing"
            smooth={true}
            offset={-50}
            duration={500}
          >
            Pricing
          </Link>
        </li>
        <div className="flex flex-col my-4">
          <button className="bg-transparent text-indigo-600 px-8 py-3 mb-4">
            Sign In
          </button>
          <button className="px-8 py-3">Sign Up</button>
        </div>
      </ul>
    </div>
  );
};

export default Navbar;
