import { CheckIcon } from '@heroicons/react/solid'
import React from 'react'


const Pricing = () => {
  return (
    <div name='Pricing' className='w-full text-white my-24'>
        <div className='w-full h-[800px] bg-slate-900 absolute mix-blend-overlay'></div>
        <div className='max-w-[1240px] mx-auto py-12'>
            <div className='text-center py-8 text-slate-300'>
                <h2 className='text-3xl uppercase'>Pricing</h2>
                <h3 className='text-5xl font-bold text-black bg-orange-100 shadow-sm rounded-2xl py-8'>The right price for you!</h3>
                <p className='text-3xl'>MEMBERSHIPS MADE TO FIT YOUR LIFESTYLE AND WALLET</p>
            </div>
            <div className='grid md:grid-cols-2'>
            <div className='bg-white text-slate-900 m-4 p-8 rounded-xl shadow-2xl relative'>
                    <span className='uppercase px-3 py-1 bg-indigo-200 text-indigo-900 rounded-2xl text-sm'>Feel Good</span>
                    <div>
                        <p className='text-6xl font-bold py-4 flex'>TBD<span className='text-xl text-slate-500 flex flex-col justify-end'>/mo</span></p>
                    </div>
                    <p className='text-2xl py-8 text-slate-500'>Entry level membership.  Get the direction and insiration you need to start your wokrout journey.</p>
                    <div className='text-2xl relative'>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to ALL Workouts</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to Group Exercise Classes</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to Cycle Classes and Groups</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to Yoga Virtual Classes</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Fit Start Personal Training Session</p>
                        <button className='w-full py-4 my-4'>Get Started</button>
                    </div>
                </div>
                <div className='bg-white text-slate-900 m-4 p-8 rounded-xl shadow-2xl relative'>
                    <span className='uppercase px-3 py-1 bg-indigo-200 text-indigo-900 rounded-2xl text-sm'>GAINZ</span>
                    <div>
                        <p className='text-6xl font-bold py-4 flex'>TBD<span className='text-xl text-slate-500 flex flex-col justify-end'>/mo</span></p>
                    </div>
                    <p className='text-2xl py-8 text-slate-500'>The Ultimate in GAINZ Theology, become a God and Godess of IRON. Eat the Mountains, drink the SEAS! These workouts will push you to places you have never dreamt about.  Are you ready?</p>
                    <div className='text-2xl relative'>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to ALL Workouts</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to Group Exercise Classes</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to Cycle Classes and Groups</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Access to Yoga Virtual Classes</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Fit Start Personal Training Session</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Discounts Codes for Supplements</p>
                        <p className='flex py-4'><CheckIcon className='w-8 mr-5 text-green-600'/>Free Merch!</p>
                        <button className='w-full py-4 my-4'>Get Started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Pricing