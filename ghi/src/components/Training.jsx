import React from 'react'

import {
  PhoneIcon, ArrowSmRightIcon
}from '@heroicons/react/outline';
import {
  UserGroupIcon, SupportIcon
}from '@heroicons/react/solid'


import liftingImg from '../assets/workingout01.webp'

const Training = () => {
  return (
    <div name='Training' className='w-full mt-24'>
      <div className='w-full h-[700px] bg-gray-900/90 absolute'>
        <img className='w-full h-full object-cover mix-blend-overlay' src={liftingImg} alt="/" />
      </div>
      <div className='max-w-[1240px] mx-auto text-white relative'>
        <div className='px-4 py-12'>
          <h2 className='pt-8 text-slate-300 uppercase text-center'>Trainer</h2>
          <h3 className='text-5xl font-bold py-6 text-center'>Finding the right workout!</h3>
          <p className='py-4 text-3xl text-slate-300'>Finding the right direction in with your workout goals can be difficult.  We are here to help reach your goals together!  Call, email or live chat with any of our team to get the information you need to feel confident in your next visit to the gym.</p>
        </div>
        <div className='grid grid-cols-1 lg:grid-cols-3 relative gap-x-8 gap-y-16 px-4 pt-12 sm:pt-20 text-black'>
          <div className='bg-white rounded-xl shadow-2xl'>
            <div className='p-8'>
              <PhoneIcon className='w-16 p-4 bg-indigo-600 text-white rounded-lg mt-[-4rem]' />
              <h3 className='font-bold text-2xl my-6'>Trainer</h3>
              <p className='text-gray-600 text-xl'>Contact a trainer today!</p>
            </div>
            <div className='bg-slate-100 pl-8 py-4'>
              <p className='flex items-center text-indigo-600'>Contact Us <ArrowSmRightIcon className='w-5 ml-2'/></p>
            </div>
          </div>
          <div className='bg-white rounded-xl shadow-2xl'>
            <div className='p-8'>
              <SupportIcon className='w-16 p-4 bg-indigo-600 text-white rounded-lg mt-[-4rem]' />
              <h3 className='font-bold text-2xl my-6'>Technical Support</h3>
              <p className='text-gray-600 text-xl'>Contact a team member today!</p>
            </div>
            <div className='bg-slate-100 pl-8 py-4'>
              <p className='flex items-center text-indigo-600'>Contact Us <ArrowSmRightIcon className='w-5 ml-2'/></p>
            </div>
          </div>
          <div className='bg-white rounded-xl shadow-2xl'>
            <div className='p-8'>
              <UserGroupIcon className='w-16 p-4 bg-indigo-600 text-white rounded-lg mt-[-4rem]' />
              <h3 className='font-bold text-2xl my-6'>Trainee Support</h3>
              <p className='text-gray-600 text-xl'>Contact a team member today!</p>
            </div>
            <div className='bg-slate-100 pl-8 py-4'>
              <p className='flex items-center text-indigo-600'>Contact Us <ArrowSmRightIcon className='w-5 ml-2'/></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Training

