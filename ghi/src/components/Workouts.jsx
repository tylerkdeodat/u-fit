import { CheckIcon } from '@heroicons/react/outline'
import React from 'react'
import { Link as RouterLink} from "react-router-dom";

const Workouts = () => {
    return (
        <div name='Workouts' className='w-full my-32'>
            <div className='max-w-[1240px] mx-auto px-2'>
                <h2 className='text-5xl font-bold text-center'>Find the right workout for you!</h2>
                <p className='text-2xl py-8 text-gray-500 text-center'>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora expedita eius ab autem consequatur suscipit nihil debitis obcaecati quos eum.</p>
                
                <div className='grid sm:grid-cols-2 lg:grid-cols-4 gap-4 pt-4'>

                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                            <h3><RouterLink to='/exercise/1431'><button className='font-bold text-lg text-black bg-orange-200'> Back</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Back</h3> */}
                            <p className='text-lg pt-2 pb-4'>In order to develop a strong back, it is important to build width and thickness to this muscle. The back is divided into four distinct muscle groups: Lats, Lower Lats, Middle Back, Lower Back.</p>
                        </div>
                    </div>
                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                            <h3><RouterLink to='/exercise/3672'><button className='font-bold text-lg text-black bg-orange-200'> Cardio</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Cardio</h3> */}
                            <p className='text-lg pt-2 pb-4'>Cardio exercise, which is sometimes referred to as aerobic exercise, is any rhythmic activity that raises your heart rate into your target heart rate zone. This is the zone where you burn the most fat and calories.</p>
                        </div>
                    </div>                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                            <h3><RouterLink to='/exercise/1254'><button className='font-bold text-lg text-black bg-orange-200'> Chest</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Chest</h3> */}
                            <p className='text-lg pt-2 pb-4'>The chest muscles of humans consist primarily of the pectoralis major and minor muscles. The pectoralis major muscle is a thick muscle that makes up a large portion of the chest muscles, while the pectoralis minor muscle is a thin muscle that lies beneath the pectoralis major.</p>
                        </div>
                    </div>                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                        <h3><RouterLink to='/exercise/0968'><button className='font-bold text-lg text-black bg-orange-200'> Arms</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Arms</h3> */}
                            <p className='text-lg pt-2 pb-4'>Exercises for the arms are designed to maintain, and in some circumstances increase, strength within the arms and upper body, along with helping to tone and shape the arms. Women often use arm exercises to maintain tone and avoid excess fat accumulation.</p>
                        </div>
                    </div>                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                        <h3><RouterLink to='/exercise/0987'><button className='font-bold text-lg text-black bg-orange-200'> Legs</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Legs</h3> */}
                            <p className='text-lg pt-2 pb-4'>When it comes to designing an effective leg workout, simpler is better. The basic lower body movements — squats, hip hinges (deadlifts), and lunges — should comprise the majority of your programming.</p>
                        </div>
                    </div>                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                        <h3><RouterLink to='/exercise/0997'><button className='font-bold text-lg text-black bg-orange-200'> Shoulder</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Shoulder</h3> */}
                            <p className='text-lg pt-2 pb-4'>Building defined shoulders can further improve a person's overall aesthetics and drastically enhance one's physique. Not to mention, the shoulders are responsible for a wide range of upper body motions. They also play a significant role in maintaining the functional and structural integrity of both men and women.</p>
                        </div>
                    </div>                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                        <h3><RouterLink to='/exercise/1011'><button className='font-bold text-lg text-black bg-orange-200'> Waist</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Waist</h3> */}
                            <p className='text-lg pt-2 pb-4'>The obliques are the muscles located along the sides of the abdominal wall. These muscles are responsible for side bending and waist twisting moves. Working the obliques helps to sculpt and cinch the waist, tones the abdominal wall and tightens the midsection.</p>
                        </div>
                    </div>                    <div className='flex'>
                        <div>
                            <CheckIcon className='w-7 mr-4 text-green-600' />
                        </div>
                        <div>
                            <h3><RouterLink to='/exercise/1403'><button className='font-bold text-lg text-black bg-orange-200'> Neck</button></RouterLink></h3>
                            {/* <h3 className='font-bold text-lg'>Neck</h3> */}
                            <p className='text-lg pt-2 pb-4'>The best way to prevent injury is by having strong, flexible muscles and joints that resist strain and injury. The back and neck like movement. Putting the back in a static position for long periods of time, such as sitting at a computer screen for hours, increases the risk of back or neck strain. The best preventive medicine for neck and back strain is movement.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Workouts