import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Stack, Typography } from '@mui/material';

const REACT_APP_EXERCISE_API_KEY = process.env.REACT_APP_EXERCISE_API_KEY

const ExerciseCard = ({ exercise }) => {
  const [current_exercise, setCurrentExercise] = useState({
    'bodyPart': '',
    'equipment': '',
    'gifUrl': '',
    'id': '',
    'name': '',
    'target': '',
  });
  const [workoutData, setWorkoutData] = useState({
    'name': '',
    'reps': '',
    'sets': '',
  });
  const [clicked, setClicked] = useState(false)

  const handleClick = async (event) => {
    setClicked(true)
    event.preventDefault();
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': 'eda12c2ef1mshdfbd798d2833a20p186fb2jsnb1744f6c3e15',
        'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com'
      }
    };
    fetch(`https://exercisedb.p.rapidapi.com/exercises/exercise/${exercise.id}`, options)
      .then(response => response.json())
      .then(exercise => setCurrentExercise({
        'bodyPart': exercise.bodyPart,
        'equipment': exercise.equipment,
        'gifUrl': exercise.gifUrl,
        'id': exercise.id,
        'name': exercise.name,
        'target': exercise.target,
      }, () => console.log(current_exercise)))
      .catch(err => console.error(err));
      console.log(exercise)
      console.log(current_exercise)
      const request = {
        method: 'POST',
        body: JSON.stringify({...workoutData, current_exercise}),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      fetch('http://localhost:8000/fitness-model/workouts', request)
        .then(response => response.json())
        .then(exercise => setCurrentExercise({
          'bodyPart': exercise.bodyPart,
          'equipment': exercise.equipment,
          'gifUrl': exercise.gifUrl,
          'id': exercise.id,
          'name': exercise.name,
          'target': exercise.target,         
        }, () => console.log(current_exercise)))
      console.log({...workoutData, current_exercise});
  };

  return (
    <>
    {clicked ? (
    <Link className="exercise-card" to={`/exercise/${exercise.id}`}>
      <img src={exercise.gifUrl} alt={exercise.name} loading="lazy" />
      <Stack direction="row">
        <Button sx={{ ml: '21px', color: '#fff', background: '#FFA9A9', fontSize: '14px', borderRadius: '20px', textTransform: 'capitalize' }}>
          {exercise.bodyPart}
        </Button>
        <Button sx={{ ml: '21px', color: '#fff', background: '#FCC757', fontSize: '14px', borderRadius: '20px', textTransform: 'capitalize' }}>
          {exercise.target}
        </Button>
        <Button onClick={handleClick} sx={{ ml: '21px', color: '#fff', background: '#00aaaa', fontSize: '14px', borderRadius: '20px', textTransform: 'capitalize' }}>
          Add to custom workout
        </Button>
      </Stack>
      <Typography ml="21px" color="#000" fontWeight="bold" sx={{ fontSize: { lg: '24px', xs: '20px' } }} mt="11px" pb="10px" textTransform="capitalize">
        {exercise.name}
      </Typography>
    </Link>
  ) : (
    <>
    <Link className="exercise-card" to={`/exercise/${exercise.id}`}>
      <img src={exercise.gifUrl} alt={exercise.name} loading="lazy" />
      <Stack direction="row">
        <Button sx={{ ml: '21px', color: '#fff', background: '#FFA9A9', fontSize: '14px', borderRadius: '20px', textTransform: 'capitalize' }}>
          {exercise.bodyPart}
        </Button>
        <Button sx={{ ml: '21px', color: '#fff', background: '#FCC757', fontSize: '14px', borderRadius: '20px', textTransform: 'capitalize' }}>
          {exercise.target}
        </Button>
        <Button onClick={handleClick} sx={{ ml: '21px', color: '#fff', background: '#00aaaa', fontSize: '14px', borderRadius: '20px', textTransform: 'capitalize' }}>
          Add to custom workout
        </Button>
      </Stack>
      <Typography ml="21px" color="#000" fontWeight="bold" sx={{ fontSize: { lg: '24px', xs: '20px' } }} mt="11px" pb="10px" textTransform="capitalize">
        {exercise.name}
      </Typography>
    </Link>
  </>
   )}
  </>
  );
}

export default ExerciseCard;

// test file for edit