import React from 'react';
import { Box } from '@mui/material';

import ExerciseCard from './ExerciseCard';
import BodyParts from './BodyParts';


const Horizontalbar = ({ data, bodyParts, setBodyPart, bodyPart }) => (
  <>
    {data?.slice(0, 3)?.map((item) => (
      <Box
        key={item.id || item}
        itemId={item.id || item}
        title={item.id || item}
        m="0 40px"
      >
        {bodyParts ? <BodyParts item={item} bodyPart={bodyPart} setBodyPart={setBodyPart} /> : <ExerciseCard exercise={item} /> }
      </Box>
    ))}
  </>
);

export default Horizontalbar;