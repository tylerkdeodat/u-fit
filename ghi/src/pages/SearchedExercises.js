import React, { useState } from 'react';
import { Box } from '@mui/material';

import Exercises from '../exercise_components/Exercises';
import SearchExercises from '../exercise_components/SearchExercises';


const SearchedExercises = () => {
  const [exercises, setExercises] = useState([]);
  const [bodyPart, setBodyPart] = useState('all');

  return (
    <Box>
      <SearchExercises setExercises={setExercises} bodyPart={bodyPart} setBodyPart={setBodyPart} />
      <Exercises setExercises={setExercises} exercises={exercises} bodyPart={bodyPart} />
    </Box>
  );
};

export default SearchedExercises;