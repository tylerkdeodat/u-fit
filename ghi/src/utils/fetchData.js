
const REACT_APP_EXERCISE_API_KEY = process.env.REACT_APP_EXERCISE_API_KEY

export const exerciseOptions = {
  method: 'GET',
  headers: {
    'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com',
    'X-RapidAPI-Key': 'eda12c2ef1mshdfbd798d2833a20p186fb2jsnb1744f6c3e15',
  },
};

export const youtubeOptions = {
  method: 'GET',
  headers: {
    'X-RapidAPI-Host': 'youtube-search-and-download.p.rapidapi.com',
    'X-RapidAPI-Key': 'eda12c2ef1mshdfbd798d2833a20p186fb2jsnb1744f6c3e15'
  },
};

export const fetchData = async (url, options) => {
  const res = await fetch(url, options);
  const data = await res.json();

  return data;
};