import React, { useEffect, useState } from 'react';
import ExerciseCard from './exercise_components/ExerciseCard';
import { Box, Stack, Typography } from '@mui/material';

export default function WorkoutForm() {
    const [workouts, setWorkouts] = useState([]);
    // const [current_exercise, setCurrentExercise] = useState({
    //     'bodyPart': '',
    //     'equipment': '',
    //     'gifUrl': '',
    //     'id': '',
    //     'name': '',
    //     'target': '',
    //   });
    //   const [workoutData, setWorkoutData] = useState({
    //     'name': '',
    //     'reps': '',
    //     'sets': '',
    //   });

    const [name, setName] = useState('');
    const [reps, setReps] = useState('');
    const [sets, setSets] = useState('');

        // async function loadWorkouts() {
        //     const request = {
        //         method: 'GET',
        //         headers: {
        //             'Content-Type': 'application/json',
        //         },
        //     };
        //     fetch('http://localhost:8000/fitness-model/workouts', request)
        //         .then(response => response.json())
        //         .then(response => setWorkout(response))
        //         .catch(err => console.error(err));
        //         console.log(workout)
        // }

        // useEffect(() => {
        //     loadWorkouts()
        // }, []);
        useEffect(() => {
            fetch('http://localhost:8000/fitness-model/workouts/')
                .then(response => response.json())
                .then(response => setWorkouts(response.workouts))
                .catch(err => console.error(err));
        }, []);
        console.log(workouts)

        // {Array.from(workouts).map(exercise => {
        //     console.log(exercise)
        //     return (

        const mappedWorkouts = workouts.map(({sets, reps, id}) =>
             ({
                sets,
                reps,
                id,
            }))
            console.log(mappedWorkouts)

        // console.log(workouts.exercise);
        // console.log(workouts[0])
            const handleChange = event => {
                console.log(event.target.value)
                const request = {
                    method: 'PUT',
                    body: JSON.stringify(workouts),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                mappedWorkouts.forEach((workouts) => {
                    console.log(workouts)
                fetch(`http://localhost:8000/fitness-models/workouts/${workouts.id}`, request)
                    .then(response => response.json())
                    .then(response => setWorkouts(response.workouts.sets, response.workouts.reps, ))

                })
                console.log(workouts)
                console.log(event.target.value)
                console.log(workouts.sets, workouts.reps)
            };
            console.log(workouts)

        return (
            <>
                <Box id="workouts" sx={{ mt: { lg: "109px" } }} mt="50px" p="px">
                    <Typography variant="h4" fontWeight="bold" sx={{ fontSize: { lg: '40px', xs: '30px' } }} mb="40px" textAlign="center" >Showing Results</Typography>
                    <Stack direction="row" sx={{ gap: { lg: "107px", xs: "50px" } }} flexWrap="wrap" justifyContent="center">
                        {workouts.map((exercise) => (
                            console.log(exercise.current_exercise),
                            <ExerciseCard key={exercise.current_exercise.id} exercise={exercise.current_exercise} />
                        ))}
                    </Stack>
                </Box>
                {/* <div className="row">
                    <table className="table table-striped" key = "table">
                        <tbody>
                        {Array.from(workouts).map(exercise => {
                            console.log(exercise)
                            return (
                            <tr key={exercise.id}>
                                <th>Name</th>
                                    <td>{ exercise.current_exercise.name}</td>
                                <th key={exercise.sets}>Sets</th>
                                    <td>
                                        <input type="text" />
                                        <button onClick={handleChange}>Add</button>
                                    </td>
                                <th>Reps</th>
                                    <td>{ exercise.reps}</td>
                                <td>
                                </td>
                            </tr>
                            );
                            })}
                        </tbody>
                    </table>
                </div> */}
            </>
        )
}