## July 11, 2022

After getting the repository created and putting a few files/folders to give the project some shape, I started working on getting the docker up and running so we would have an stable container to work from. After I was able to get the docker container working I started working on the backend with FastAPI. Getting a base model set up to work with. I connected the fastapi to the docker-compose.

## July 12, 2022

To bolster the app and to give it more structure, I created the react app that will be the frontend with a fastapi and mongodb backend. I created small test case by pulling the data that we want to use form an outside api and displayed it in a table. I then started working on a search function to lookup specific terms used in the imported api to filter exercises.

## July 13 & 14, 2022

After getting what I felt like a good base of the app, I moved back to the backend to work on getting our database to work. I was getting weird errors about not being authenticated, then when that was fixed, I was getting errors about the information not being in a dict. I struggled for a while then got help from everyone that works at hack reactor. We were finally able to get something working but it was up to me to get the rest of it going and with some more struggling I was bale to get a basic crud operation going.

## July 15, 2022

After finally getting the database to properly, work, I shifted gears and started helping Tyler implement the code that he was working on which was the actual data structure that we would be using for our project. We spent the rest of the day integrating the code that we worked on to properly handle crud operations.

## July 18, 2022

I started the day by catching up on my journalizing. Started working on authentication. I did the fastapi tutorial on authentication and started trying to incorporate those lessons to what was given to use in the learn.

## July 19, 2022

Continued trying to figure out how to incorporate auth. No luck.

## July 20, 2022

Continued working on auth, changed some of the models to work better with what i was trying to ac
accomplish. Tried switching from a user model to working with the existing client models we created. Not a lot of progress. Hid the api keys that we are using.

## July 21-25, 2022

Continued working on auth. Started by trying to incorporate the client model and client queries to make auth work. Abandoned that idea and created an accounts queries and user model to get a person to login with fastapi. Made some progress, as I was able to check to see if the user trying to login existed in the database.

## July 25-28, 2022

Continued working on auth. Since I was abe to get the user model and the account queries to work, I decided to go back to the drawing board to see if I could use the existing client models again. I was able to get that working using the client model with the account queries. Changed some of the client models ot better represent the data i was trying to pass in.

## July 30, 2022

Still working on auth. Made some progress on the backend. Started trying to incorporate the frontend auth with the backend. No real success, except I was able to verify existing users, sign them in, and started working on a logout page.

## July 31, 2022

Cleaned up some code that was dry. Created gym, date, and time models and routes to be able to add this data to the database and display this information to the user. Worked on auth some more.

## August 1, 2022

Worked on the existing models so that nested data would be able to accurately reflect there parent owner. Made it so data that was linked to other data could be properly displayed when called.

## August 2, 2022

Continued to work on the models, making sure that data in matched data out. Helped Tyler with the workout queries. Refactored the login/logout to flow better and not clutter up a single page and could be pulled in from another file.

## August 3, 2022

Finish working on the models. Now they all work as expected. Created a nice UI for the exercise search and cards to display the information in a pleasing way. Continued back with auth.

## August 4, 2022

Auth, Auth, and more Auth. Nothing. :'(
Updated the docs and some Readme. Cried myself to sleep.

## August 5, 2022

Last day of the project. Will be doing a unit test. Making sure the journal was updated properly. Making sure the readme looks good, and then will try to get the auth working. Fingers crossed. Happy grading. Finally got auth to work. Helped touch up the UI and make a newer login/logout form.
