## August 5, 2022

Today I got our pages to work properly with the navbar and app.js.  I added links to the signup/login/logout buttons.  Made the login and logout buttons recognize if there was a token and if there was making the login and signup button become hidden only displaying the logout option.  I made all the buttons that scrolled also navigate to the main page wiht a ternary operator.  I worked on more css updates to pages fixing padding, margins and other size issues.  Added bg color and btn color.  Finishing up small cleaning items and journals.  going to do final push! Added a signup btn on the sign in page

## August 4, 2022
Today I merged the two nav bars we had together.  I also converted the mainpage into the single page style app page I had created using tailwindcss and jsx.  Added a ternary operator to the Home button so it would scroll and navigate to the mainpage ('/').  Started building my unit tests. corrected an error I had with a simple async.  Added a footer to the main page as well and scroll to nav bar buttons. 

## August 3, 2022

Today i finished the main page rebuild to have a few different sections that you can scroll through and I also made the main page so you can view it on mobile devices and have a hidden menu appear.  corrected an error I had with a simple async.  

## August 2, 2022

Today I continued working on the main page rebuild with Tailwindcss and JSX I am adding icons from Heroicons and learning some more tailwindcss features.  Fixed some issues with the Nav and App file also helped update teh fitness model files. 

## August 1, 2022

Started my day with rebuilding the main page using Tailwindcss and Jsx.  

## July 28, 2022

Corrected small errors with my JS files from the day before.  Updated the Nav.js form.  Continued looking into Tailwindcss, started a small side project to try some things out.  
## July 27, 2022

Cleaned up the Nav and App file.  I finished the About, Otherinfo, Personalinfo, Signup, and SignupInfo JS forms. I also updated the yaml file and fixed some issues on our fitness model.  Started to mess around with TailwindCSS for our project.  
## July 26, 2022

Today I am still working on front end Authentication.  I started workingo n some JS forms for signing up users on the front end.  Did not complete them so did not commit today.  

## July 25, 2022

Worked with Tony on getting Nav issues resolved and give any input with Tyler and Ethan on Auth work.  Small victory!  Got a token back from fastAPI.  

## July 22, 2022

Spoke with the team going to review some items in learn but did not having any large changes on the project.  Did not commit anything.  Going to continue to review and try and figure out some issues with our token.  

## July 21, 2022

Attempted to help the team with getting some movement with Auth, did not go very well.  We are still fairly stuck.  We were able to get some life shown by seeing if a user was trying to login existed in the database so some small progress.  

## July 20, 2022

I attempted to work on the Auth work but got way too confused and stuck so sent my work to the team so they could take a crack at it.  Live coded with him to try and get something going on Auth.  Created new auth functions and classes. 

## July 19, 2022

Spent time researching on Auth and more FastAPI information.  Went through and touched up some things on the Trainee/Trainer form, still have a lots of bugs.  Auth is not going well might need further assistance in getting it running.  

## July 18, 2022

Today I started on a few JS forms to help us create Trainers and Trainees.  I also pulled some old work from carcar and am attempting to morph it into working code for our current projects.  Issues converting the Nav and mainpage js form.  Probably will end up starting with scratch but going to continue to work with it for now.  I also corrected issues we were having with the workout models and small changes to the data-model.md


## July 15, 2022

Today I helped live code db.py and routes.  We got data to show up from our fastapi/mongo express calls on the trainer model! Finishled the day up with writing the code for handling CRUD operations. 
## July 14, 2022

Worked with Tony to make the Client.py.  Then tried to help Tyler figure out issues with the Appointment models.  We cannot seem to figure out issues with the routes and queries.  Going to try and commit something tonight but code is still broken.  

## July 13, 2022

I went through the learn and read docs and watched a few youtube videos on how FastAPI works, still a bit confused on it.  Videos were helpful on Youtube, messed around in a side VScode project to test out what they were going over.  

## July 12, 2022

Paired programmed with Tyler to walk through some fastAPI and mongoDB ideas.  Started the models and discussed how we wanted to config our models to meet our projects needs of Trainers and Trainees


## July 11, 2022

After getting the repository created and putting a few files/folders to give the project some shape, We started working on getting the docker up and running so we would have an stable container to work from. After I was able to get the docker container working we started working on the backend with FastAPI. Getting a base model set up to work with. we connected the fastapi to the docker-compose.  We also worked on our excelidraw to map out our project.