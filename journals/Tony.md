## August 5, 2022
    Messed up merging with main trying to fix. and got tests to run.
## August 4, 2022
    Still writing the tests.  Still getting help on authentication. 
## August 3, 2022
    Getting help on Authentication. Trying to decide how the site should look like.
## August 2, 2022
    Looking at how to write tests.  Screenshared writing tests. Got help with some backend routers and db items.
## August 1, 2022
    Team screenshared trying to get exercise is and certain infor to show to appen the workout model.
## July 27, 2022
    More front end stuff. Authentication still not working. Maybe some SEIR time tomorrow?
## July 26, 2022
    Looking at Tailwind for making the site look pretty.  Overall, looks good but Authentication still not working.
## July 25, 2022
    Trying to figure out the user authentication to work with team.
## July 22, 2022 
    Reading up on the FARM Stack.  Did not touch project
## July 20, 2022
    Tyler and I fixed the client issue.  Read up on OAuth2 documentation for fastapi. 

## July 19, 2022
    Retouched to Clients and routes. but running into some errors.
## July 15, 2022
    Worked on the db.py and routers.  We finally got fastapiand mongoexpress to show data for the trainer model. We worked on writing the code for handling CRUD operations.
## July 14, 2022
    Started on Client.py, but can't seem to get it right. 
## July 13, 2022
    Decided to go with a Fastapi and mongoDB backend. Liveshared with team to work on a function to lookup certain exercise terms.
## July 12, 2022
    We worked together on routers for Trainer. Trying to figure out if we want to use a django or fastapi for the project.