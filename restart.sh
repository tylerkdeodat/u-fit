#! /bin/bash

docker-compose down
docker volume remove mongo-data
docker volume create mongo-data
docker-compose up