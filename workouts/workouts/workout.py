from pydantic import BaseModel

class WorkoutIn(BaseModel):
    name: str
    equipment: str
    phone_number: str
    age: int
    gender: str

class WorkoutOut(WorkoutIn):
    id: str

class Workout(BaseModel):
   workouts: list[WorkoutOut]
